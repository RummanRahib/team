# Team

Exercises and tools from developing our team's and interns' technical skills:

Linux (`bash`)
Python
Machine Learning (`sklearn`)
NLP (`spacy` and `torch`)
Conversation design (`qary`)
Databases (`SQL` and `Django` ORM)
