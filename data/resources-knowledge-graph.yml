# Datasets for Knowledge Graphs, machine learning and NLP
#
# SEE ALSO:
#   qary repository: src/qary/data/datasets.yml
#   nlpia2 repository: src/nlpia2/data/datasets.yml or resources-datsets-data-source.yml
#   team repository: data/exercises/2-data/resources-datasets-data-sources.yml
#
# Knowledge-graphs
# - Knowledge-graph Inference
# - Knoweldge Extraction for populating Knowledge-graphs
# - Grounded dialog engines
-
  title: "Knowledge Extraction and Inference from Text: Shallow, Deep and Everything in Between (Tutorial at SIGKDD)"
  urls:
    - https://docs.google.com/viewer?a=v&pid=sites&srcid=ZGVmYXVsdGRvbWFpbnxrZWl0MjAxOGtkZHxneDoyNjNkYjY4ZTY2OGNhNmMx
    - https://docs.google.com/viewer?a=v&pid=sites&srcid=ZGVmYXVsdGRvbWFpbnxrZWl0MjAxOGtkZHxneDoxYTM5YmExOWVmY2I4NDc3
  filenames:
    - KDD2018-Part1.pdf
    - KDD2018-Part2.pdf
  author:
    name: Partha Talukdar
    org: IISc Bangalore and Kenome
    email: ppt@iisc.ac.in
-
  title: KILT Benchmarking
  description: KILT is a resource for training, evaluating and analyzing NLP models on Knowledge Intensive Language Tasks.
  url: https://ai.facebook.com/tools/kilt
  author:
    org: facebook
-
  url: https://arxiv.org/pdf/2002.00388v1.pdf
  title: A Survey on Knowledge Graphs
  filename: 2002.00388v1.pdf
-
  title: The Knowledge Graph Cookbook -- Recipes that Work
  filename: the-knowledge-graph-cookbook.pdf
-
  title: Knowledge Aware Conversation Generation with Explainable Reasoning over Augmented Graphs
  filename: 1903.10245v4.pdf
  url: https://arxiv.org/abs/1903.10245v4
  abstract: Two types of knowledge, triples from knowledge graphs and texts from documents, have been studied for knowledge aware open-domain conversation generation, in which graph paths can narrow down vertex candidates for knowledge selection decision, and texts can provide rich information for response generation. Fusion of a knowledge graph and texts might yield mutually reinforcing advantages, but there is less study on that. To address this challenge, we propose a knowledge aware chatting machine with three components, an augmented knowledge graph with both triples and texts, knowledge selector, and knowledge aware response generator. For knowledge selection on the graph, we formulate it as a problem of multi-hop graph reasoning to effectively capture conversation flow, which is more explainable and flexible in comparison with previous work. To fully leverage long text information that differentiates our graph from others, we improve a state of the art reasoning algorithm with machine reading comprehension technology. We demonstrate the effectiveness of our system on two datasets in comparison with state-of-the-art models.
-
  url: "https://www.aclweb.org/anthology/D19-1187.pdf"
-
  title: "Knowledge Aware Conversation Generation with Explainable Reasoning
  over Augmented Graphs"
  abstract: "Two types of knowledge, triples from knowledge graphs and texts from documents, have been studied for knowledge aware opendomain conversation generation, in which graph paths can narrow down vertex candidates for knowledge selection decision, and texts can provide rich information for response generation. Fusion of a knowledge graph and texts might yield mutually reinforcing advantages, but there is less study on that. To address this challenge, we propose a knowledge aware chatting machine with three components, an augmented knowledge graph with both triples and texts, knowledge selector, and knowledge aware response generator. For knowledge selection on the graph, we formulate it as a problem of multi-hop graph reasoning to effectively capture conversation flow, which is more explainable and flexible in comparison with previous work. To fully leverage long text information that differentiates our graph from others, we improve a state of the art reasoning algorithm with machine reading comprehension technology. We demonstrate the effectiveness of our system on two datasets in comparison with state-of-the-art models1."
  authors:
    - "Zhibin Liu (liuzhibin05@baidu.com)"
    - Zheng-Yu Niu (niuzhengyu@baidu.com)
    - Hua Wu (wu_hua@baidu.com)
    - Haifeng Wang (wanghaifeng@baidu.com)
  org: Baidu Inc., Beijing, China
-
  title: "A Survey on Knowledge Graphs: Representation, Acquisition and Applications"
  authors:
    - Shaoxiong Ji
    - Shirui Pan
    - Erik Cambria
    - Pekka Marttinen
    - Philip S. Yu
  abstract: Human knowledge provides a formal understanding of the world. Knowledge graphs that represent structural relations between entities have become an increasingly popular research direction towards cognition and human-level intelligence. In this survey, we provide a comprehensive review on knowledge graph covering overall research topics about 1) knowledge graph representation learning, 2) knowledge acquisition and completion, 3) temporal knowledge graph, and 4) knowledge-aware applications, and summarize recent breakthroughs and perspective directions to facilitate future research. We propose a full-view categorization and new taxonomies on these topics. Knowledge graph embedding is organized from four aspects of representation space, scoring function, encoding models and auxiliary information. For knowledge acquisition, especially knowledge graph completion, embedding methods, path inference and logical rule reasoning are reviewed. We further explore several emerging topics including meta relational learning, commonsense reasoning, and temporal knowledge graphs. To facilitate future research on knowledge graphs, we also provide a curated collection of datasets and open-source libraries on different tasks. In the end, we have a thorough outlook on several promising research directions.
