# Connect All San Diego

Apply for FREE 4-month business accelerator program [here](http://connectallsd.org/) to learn more.

## Board of Directors

### Valerie Jacobs Hapke

![valerie-headshot-edit](https://www.jacobscenter.org/wp-content/uploads/2022/02/valerie-headshot-edit-200x250.jpg "valerie-headshot-edit")

Daughter of Joe and Vi, Valerie is the chairman of the board for the Jacobs Center for Neighborhood Innovation and Jacobs Family Foundation.

Valerie is also the founder of [Valerie Jacobs Consulting](http://www.valeriejacobs.com/), dedicated to providing direct consultation support to individual philanthropists and family foundations. In addition, Valerie is a nationally-known speaker and has created a series of workshops for women of wealth and wealthy families dealing with both philanthropy and family dynamics. She has been a licensed psychotherapist since 1979, specializing in the effects of wealth on relationships and personal well-being, and has worked in the field of philanthropy since 1994.

Valerie holds a Bachelor of Arts degree in anthropology from the University of California, Santa Barbara and a Master of Education from California State Polytechnic University, San Luis Obispo.

### Norman “Norm” F. Hapke, Jr.

![NormHapke-200x250](https://www.jacobscenter.org/wp-content/uploads/2015/02/NormHapke-200x2501.jpg "NormHapke-200x250")

Norm Hapke is a board member of the Jacobs Family Foundation and Jacobs Center for Neighborhood Innovation. Norm spent nine years as a U.S. Marine Corps Officer, first serving as an infantry officer in Vietnam and then as a naval aviator flying worldwide transport and refueling missions. After leaving the Marine Corps, he entered commercial aviation and served as a Captain with America West Airlines until his retirement in 2005.

Norm has a strong interest in public policy issues and international philanthropy and is an advocate of self-help community change strategies and school reform. His priority is helping the Jacobs Center for Neighborhood Innovation and the Jacobs Family Foundation develop the human, economic, and physical infrastructure of the Diamond Community.

Norm holds a Bachelor of Science degree in Naval Science from the U.S. Naval Academy. He is married to Valerie Jacobs Hapke, and they have two children.

### Andrew Hapke

![AndrewHapke-200x250](https://www.jacobscenter.org/wp-content/uploads/2015/02/AndrewHapke-200x2501.jpg "AndrewHapke-200x250")

Grandson of the founders and son of Valerie and Norm, Andrew Hapke was the first member of the third generation to become Board Chair of the Jacobs Family Foundation and the Jacobs Center for Neighborhood Innovation.

Professionally, Andrew coaches leaders in technology to take their careers and companies to the next level. Before he became an executive coach, he spent 15 years building products and leading teams at American Express, Etsy, and Splice. Having been a start-up founder, a product leader, and a mentor to technology executives, he has worked at the intersection of entrepreneurship, technology and social impact for his whole career.

Andrew received his MBA from Yale University, and his undergraduate business degree from Villanova University.

### Claire Hapke

![ClaireHapke-200x250](https://www.jacobscenter.org/wp-content/uploads/2015/02/ClaireHapke-200x2501.jpg "ClaireHapke-200x250")

A third generation Jacobs family member, Claire grew up watching the work of her grandparents and parents in what would become The Village at Market Creek. Her passion for learning about new cultures and appreciation for what residents have done together have led her to join the board of the family’s foundation and the Jacobs Center for Neighborhood Innovation. Claire received her doctorate in marriage and family therapy and is currently practicing in New York City.

### Juan Carlos Hernandez

![juan_carlos_hernandez_web](https://www.jacobscenter.org/wp-content/uploads/2020/01/juan_carlos_hernandez_web.jpg "juan_carlos_hernandez_web")

Juan Carlos Hernandez joined California Southern SBDC as the Chief Credit Officer/Senior Vice President in May of 2018; and was appointed as President and CEO of the organization in January 2021.

Prior to joining Cal-Southern, Juan Carlos was Chief Credit Officer of ACCION San Diego from September of 2014 and was responsible for lending staff supervision and training, program development, underwriting, loan portfolio quality and management, loss mitigation and recovery; and participating in the development of bank and community partner relations.

Juan Carlos received a B.A. in Sociology, with an emphasis in WorkForce Studies from University of Illinois at Chicago.  He is originally from Mexico City; moved to Chicago, Illinois in 1977 and has resided in San Diego since July of 2012.

Juan Carlos has over 30 years of experience in banking, including consumer, business and commercial real estate lending.  Prior employment includes working with US Bank in San Diego California.  In Chicago, he worked for Metropolitan Bank Group, La Salle Bank & Trust and The First National Bank of Chicago.  He has been featured in various newspaper articles including The Wall Street Journal.  Juan Carlos served as President for the San Diego County Hispanic Chamber of Commerce and is currently serving on the Board of JCNI, Chicano Federation and TranscenDANCE.

### Lonnie Lucas

![lonnie-lucas-headshot_cropped](https://www.jacobscenter.org/wp-content/uploads/2022/02/lonnie-lucas-headshot_cropped-200x250.jpg "lonnie-lucas-headshot_cropped")

Lonnie Lucas was born and raised in Southeast San Diego. After graduating from Gompers Secondary School, she felt the call to serve and joined the United States Army. She was awarded the Army Achievement Medal, Army Good Conduct Medal, Iraq Campaign Medal, National Defense Service Medal, Korea Defense Service Medal, Army Service Ribbon, and Global War on Terror Ribbon.

Post military, Lucas decided to pursue a degree in English Literature and subsequently studied Law. She holds a Bachelor’s degree in English Literature from the University of Pennsylvania and a Juris Doctor degree, and a certificate in Business Law from Washington University School of Law.

Lucas’ passion for community and people was evident when she was chosen to represent Washington University in South Africa in its Global Public Interest Law Program. As a Global Public interest Law Fellow, she traveled to South Africa and provided legal assistance to low-income individuals with immigration, civil rights, land rights, environmental, housing, family, and criminal matters.

Lonnie is elated to return to Southeast San Diego as a recent graduate; she is currently studying for the California Bar. Her practice will focus on business and community building.

### Louie Nguyen

![louie-nguyen_web](https://www.jacobscenter.org/wp-content/uploads/2021/03/louie-nguyen_web.jpg "louie-nguyen_web")

Louie Nguyen’s commitment to Southeastern San Diego is clear in his current role as Chief Investment Officer at Mission Driven Finance, a private impact investment boutique that provides opportunities to easily invest in the community to solve social pain points.

Through Mission Driven Finance, he recently played a key role in bringing Access Youth Academy, a nonprofit organization that helps train youth to be first-generation college students through the sport of squash – all at no cost to the area. The campus will also offer recreation spaces and meeting rooms for the general community to use for free and is set to open this summer.

Louie is also excited to bring his nearly 25 years as a skilled investor to the CONNECT ALL @ the Jacobs Center Advocacy Council, helping entrepreneurs grow their businesses and jobs within San Diego through the region’s first low-to-moderate income and diversity-focused business accelerator program. The program recently held its third Union Bank Start-Up Pitch Competition, awarding $15,000 in business grants to three diverse women-owned businesses.

### Paul Robinson

![paul_robinson_web](https://www.jacobscenter.org/wp-content/uploads/2020/01/paul_robinson_web.jpg "paul_robinson_web")

Paul Robinson is the founder of Ensunet Technology Group, a San Diego based company specializing in Mergers and Acquisitions post-merger integration and IT enterprise architecture solutions, which was featured on Inc Magazine’s 2018 list of the 5,000 fastest-growing private companies in America.

Having grown up in the Southeastern San Diego community, Paul has seen what happens when lack of economic opportunity, education, resources, community engagement, community mentorship, and crime all meet at the same intersection.

Leveraging a career spanning two decades in information technology, entrepreneurship, and bringing a community resident perspective, Paul is part of the Jacobs Center for Neighborhood Innovation board to support the organization’s education to careers and local economy initiatives. Both programs focus on creating local and systems-level partnerships that offer accessible pathways for individuals seeking education and employment. These programs also focus on creating mentorship networks that support early-stage community entrepreneurs and accelerate existing neighborhood businesses.

### Alberto “Beto” Vasquez

![alberto-vasquez-headshot](https://www.jacobscenter.org/wp-content/uploads/2022/02/alberto-vasquez-headshot-200x250.jpg "alberto-vasquez-headshot")

Dr. Beto Vasquez is a proud father of four and a resident of Southeast San Diego. He is an interdisciplinary scientist currently employed with the University of California San Diego’s (UCSD) Center for Research on Educational Equity, Assessment and Teaching Excellence (CREATE), an educational research center with equity at its core.

Through his various roles at UCSD and as an adjunct (biology) faculty at the community college, Dr. Vasquez spearheads efforts to increase diversity in STEM (Science, Technology, Engineering & Math), and access to resources for disenfranchised communities via innovative P-20 grants, initiatives, and collaborations.

Dr. Vasquez is very active in the community and has assisted with designing programs and events (supporting education, social justice, justice reform, system-impacted populations, and minorities in STEM), and uses his story (school dropout, homelessness, incarceration, and addiction); to motivate others and inspire his work. He has worked in local government, education, and the non-profit sectors.

### Reginald Jones

![reginald-jones-new-background-for-website](https://www.jacobscenter.org/wp-content/uploads/2021/01/reginald-jones-new-background-for-website-200x250.jpg "reginald-jones-new-background-for-website")

Overseeing the organization’s mission and goals since 2012, Reginald Jones is president and chief executive officer of the Jacobs Center for Neighborhood Innovation and the Jacobs Family Foundation.

Through the Jacobs Center for Neighborhood Innovation, Jones leads efforts to develop enduring partnerships that create equitable, community-driven change in Southeastern San Diego, focusing on physical development and economic opportunity. He works with culturally diverse residents, organizations, political leaders and private entities to foster understanding of the organization’s intent, seek input and build collaboration for ambitious neighborhood revitalization efforts. Current projects include CONNECT ALL @ the Jacobs Center, the region’s first low to moderate income and diversity-focused business accelerator program. A partnership between the City of San Diego, the Jacobs Center for Neighborhood Innovation and CONNECT w/ San Diego Venture Group, the program guides startups that want to grow rapidly with the support they need to be successful.

Prior to his work in San Diego, Jones served as president of the Chicago-based Steans Family Foundation, a philanthropy committed to revitalizing North Lawndale, a disinvested community on Chicago’s west side.

Jones’ leadership in philanthropy extends to serving as a board member for the Museum of Photographic Arts, advisory board member for Mainly Mozart and trustee with the Steans Family Foundation. He has served on the board of the Council on Foundations — for which he chaired the Family Philanthropy committee, Grassroots Grantmakers — as co-chairman, and the Lake County (Illinois) Community Foundation.

Jones is a frequent presenter on topics of philanthropy and placed-based driven approaches for community change. In 2019, Jones was honored with the San Diego METRO Magazine’s Men of Influence Award for his leadership in supporting innovative, practical strategies for community change in Southeastern San Diego. He is currently an Aspen Institute-Neighborhood Funders Group Philanthropy Forward Fellow.

### Joseph “Joe” J. Jacobs, Ph.D.

![JoeJacobs-200x250](https://www.jacobscenter.org/wp-content/uploads/2015/02/JoeJacobs-200x2501.jpg "JoeJacobs-200x250")

#### (In Memoriam)

The late Dr. Joe, who passed away on October 23, 2004, was the founder and director emeritus of the [Jacobs Family Foundation](http://www.jacobsfamilyfoundation.org/) and the Jacobs Center for Neighborhood Innovation.

He was chairman of the board of [Jacobs Engineering Group](http://www.jacobs.com/), an international engineering and construction firm with approximately 35,000 employees. He developed the firm from a one-man consultancy in 1947 to its present status as a public company with revenues close to $11 billion.

Inducted into the National Academy of Engineers, Joe had a distinguished career both in engineering and in humanitarian achievements, including winning the United Nations World Citizen Award in 1996.

He served on many boards of directors and worked actively toward peace in the Middle East. Joe was author of _The Anatomy of an Entrepreneur: Family, Culture and Ethics_ and _The Compassionate Conservative: Seeking Responsibility and Human Dignity._

Joe held a Ph.D. in chemical engineering from Polytechnic University.

### Violet “Vi” Jabara Jacobs

![ViJacobs-200x250](https://www.jacobscenter.org/wp-content/uploads/2015/02/ViJacobs-200x2501.jpg "ViJacobs-200x250")

#### (In Memoriam)

Vi was a founder and board member emeritus of the Jacobs Family Foundation and Jacobs Center for Neighborhood Innovation. She passed away on January 12, 2015.

She began her career in advertising. Vi was an active partner in launching the Jacobs Engineering Group, working alongside her husband, Joe, in the company’s start-up years.

During her daughters’ school years, she became involved in the PTA and actively raised money to strengthen education. She has served as a volunteer for the American Friends Service Committee, the Huntington Hospital, and Meals on Wheels, and as secretary for the Westminster Presbyterian Church.

Vi was a worldwide traveler and an advocate for economic and educational development in her parents’ homeland of Lebanon.

Vi held a bachelor of arts degree in French from Wellesley College.

### Margaret “Meg” E. Jacobs

![MegJacobs-200x250](https://www.jacobscenter.org/wp-content/uploads/2015/02/MegJacobs-200x2501.jpg "MegJacobs-200x250")

#### (In Memoriam)

The late Meg Jacobs, who passed away on February 8, 2012, was the eldest daughter of Joe and Vi Jacobs. Meg spent many years in the field of social work, with a diverse health and human services background that included work with youth, families, and victims of domestic violence. Fluent in Spanish, she was a strong advocate for the expansion of community-based social services for Spanish-speaking clients. She provided home-based services to at-risk families in a child abuse and delinquency prevention program, and assisted in the writing of a new parent education curriculum for California’s schools.

Meg served as the Jacobs Family Foundation’s first director, spearheading the foundation’s support of micro-enterprise development as a strategy for assisting families in gaining self-sufficiency. She continued to be an active participant in the foundation’s neighborhood strengthening efforts as a member of the grants team. She also assisted in hosting site visits for foundations and organizations studying Market Creek Plaza as an example of hometown philanthropy.

Meg held a bachelor of arts degree in American civilization from Brown University, and a master’s degree in social work from San Diego State University.
