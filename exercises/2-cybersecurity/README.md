# training on cybersecurity

## Git repositories

The most common devops security mistake is to accidentally commit secret security credentials (tokens, keys or passwords) or PII to a repository.
When you discover this you must quickly respond by:

### 1. Invalidate those credentials

1. Search the suspect file for passwords, tokens and keys
2. Determine which services depend on those passwords tokens and keys
3. Use your password manager (e.g. BitWardwn) to automatically change any passwords that have been compromised
4. Log into those services and delete ALL tokens and keys on those services. For example:
5. Delete the AWS/DigitalOcean tokens and keys compromised in the breach
6. Delete any other user or application tokens and keys on that service, even if they weren't mentioned in the compromised file
7. Delete the root account tokens associated with that account
8. Reset/change your user password on that service
9. Reset/change the root/billing account password on that service
10. Ensure TFA is enabled on your (root and user) accounts
11. Require all teammates to enable TFA 
12. Ask all teammates who use that service to reset their passwords, tokens and keys on that service
13. Find any related services (e.g. Github or GitLab cicd) that might have access to that service and reset their passwords, tokens, and keys
14. Turn off all servers that are managed by that service (don't wait for snapshots or other backups to complete)

Many of these steps may seem redundant. They are not. You **MUST** do them all as quickly as possible to have any confidence in recovering those accounts and making them trustworthy again.

### 2. Cleanup the rep

1. Make the repository private
2. Use one of the following tools to remove all the compromised files
    - How to use git-filter-repo to [remove files from git history](https://marcofranssen.nl/remove-files-from-git-history-using-git-filter-repo) 
    - [GitHub Docs - Removing sensitive data from a repository](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/removing-sensitive-data-from-a-repository)
    - [BFG Repo-Cleaner](https://rtyley.github.io/bfg-repo-cleaner/)
