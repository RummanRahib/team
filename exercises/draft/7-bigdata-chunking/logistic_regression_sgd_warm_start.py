import numpy as np
x = np.random.randn(1000)
y = (x > .5).astype(bool)
x[500:] *= 2
x += .3*np.random.randn(1000)
import pandas as pd
pd.DataFrame([x,y]).T
df = pd.DataFrame([x,y]).T
df.columns = 'x y'.split()
df.plot(kind='scatter')
df.plot(kind='scatter', x='x', y='y')
df.columns
df.plot?
df.plot(x='x', y='y')
df.astype(float).plot(x='x', y='y', kind='scatter')
from matplotlib import pyplot as plt
import seaborn as sns
sns.set_style()
plt.show()
from sklearn.linear_model import LogisticRegression
x1 = df[['x']].values[:500,1]
x1 = df[['x']].values[:500]
x2 = df[['x']].values[500:]
y1 = df['y'].values[:500]
y2 = df['y'].values[500:]
model = LogisticRegression()
model.fit(df[['x']], df['y'])
model.fit(df[['x']], df['y'].astype(bool))
model1 = LogisticRegression()
model2 = LogisticRegression()
model3 = LogisticRegression()
model1.fit(x1, y1.astype(bool))
model2.fit(x2, y2.astype(bool))
model3.fit(x1, y1.astype(bool))
model3.fit(x2, y2.astype(bool))
model3.coef_
model.coef_
model2.coef_
model1.coef_
model3.coef_
model3.intercept
model3.n_iter_
model3 = LogisticRegression(n_iter=1)
model3 = LogisticRegression(max_iter=1)
model3.fit(x1, y1.astype(bool))
model3.fit(x2, y2.astype(bool))
model3.fit(x2, y2.astype(bool), warmstart=True)
model3.fit(x2, y2.astype(bool), warm_start=True)
from sklearn.linear_model import SGDClassifier
sgd = SGDClassifier(warm_start=True)
sgd.fit(x1, y1.astype(bool), warm_start=True)
sgd.fit(x1, y1.astype(bool))
sgd.fit(x2, y2.astype(bool))
sgd.coef_
sgd3 = sgd; sgd = SGDClassifier(warm_start=False)
sgd3.coef_
sgd = SGDClassifier()
sgd1 = SGDClassifier()
sgd2 = SGDClassifier()
sgd3 = SGDClassifier(warm_start=True)
sgd.fit(x, y.astype(bool))
sgd.fit(x.reshape(-1,1), y.astype(bool))
sgd1.fit(x1, y1.astype(bool))
sgd2.fit(x2, y2.astype(bool))
sgd3.fit(x1, y1.astype(bool))
sgd3.fit(x2, y2.astype(bool))
sgd3.coef_
sgd.coef_
sgd2.coef_
sgd1.coef_
hist -o -p -f logistic_regression_sgd_warm_start.ipy
hist -f logistic_regression_sgd_warm_start.py
