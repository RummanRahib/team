# Python logging

## References

- [Tutorial: `structlog` & New Relic Logs](https://newrelic.com/blog/how-to-relic/python-structured-logging#toc-using-the-new-relic-logs-api)
- [solarwinds Loggly](https://www.loggly.com/)
- [DataDog](https://www.datadoghq.com/)
