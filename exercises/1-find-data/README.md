# 1-find-data
## Below is a collection of dataset catalogs:

1. https://huggingface.co/ 
A great collection of various NLP datasets and models, but with a focus on NLP and not much new uses for NLP and datasets that do not really have to do with NLP. Has a great and easy to use interface.

2. https://kaggle.com/
A great diverse collection of datasets and models with its Kaggle kernels/notebooks on all topics like Computer Vision and Natural Language Processing and time series predictions.

3. https://data.gov/ 
A collection of high quality US government collected datasets.

4. https://paperswithcode.com/ 
A collection of various research papers in the overall area of computer science and AI.

5. https://arxiv.org/
A collection of basically every research paper that exists from many different fields. Not well organized as in there is no way to search for datasets or models so you have to visit each paper that looks like it might have what you need.

6. https://opendata.cern.ch/
A collection of datasets made by CERN (a nuclear research organization)

7. https://datasetsearch.research.google.com/
A way to search datasets by Google.

8. https://www.tensorflow.org/datasets
All datasets by Tensorflow. Widely know datasets like MNIST come built in with Tensorflow.

https://datacatalog.worldbank.org/home
A collection of economic data by World Bank.

9. https://github.com/awesomedata/awesome-public-datasets
A Github repository of high quality datasets. Not on Gitlab as far as I know.
