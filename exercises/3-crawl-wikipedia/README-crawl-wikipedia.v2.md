# Crawl Wikipedia

There are at least 4 ways to download data from Wikipedia:

1. directly scrape the HTML with the `requests` package
2. use `requests` to query [the Wikipedia API](https://www.jcchouinard.com/wikipedia-api/)
3. use the [`Wikipedia`](https://gitlab.com/tangibleai/Wikipedia) package to download preprocessed text
4. use the [`Wikipedia-API` package](https://pypi.org/project/Wikipedia-API/) to download preprocessed text

## Resources

- Tangible AI's [fork of the `Wikipedia`](https://gitlab.com/tangibleai/Wikipedia) source code
- [PyPi Wikipedia-API package](https://pypi.org/project/Wikipedia-API/)
- Unmaintained [PyPi Wikipedia package](https://pypi.org/project/Wikipedia/)
- tutorial on [using python to access the web api](https://www.jcchouinard.com/wikipedia-api/)
