
## Intro

Your mission, should you choose to accept it, is to find the Data Science treasure chest.
If you know how to run the "gradient descent" algorithm in your head, you can skip the [Assignment Instructions](#instructions) section at the end.
This will help you understand how to make guesses for model parameters that minimize the error in your "model".
The closer your model is to the "truth" the smaller the RMSE (Root Mean Square Error) will be.

## Child's play

It's like a game of [Marco Polo](Marco_Polo_(game)) or [Hunt the Thimble](https://en.wikipedia.org/wiki/Hunt_the_thimble) where you are only given clues as to where the treasure is.
Or it's like hunting for invisible treasure on a 2-D treasure map in a DnD adventure or roll playing video game.
With each guess, your are only told how far you are from the treasure.
You have a metal detector that beeps to tell you how far you are from the treasure.

## Data Scientist's play too

This is an adult game that engineers get paid big bucks to solve every day. 
The treasure chest is usually a machine learning model that businesses can use to predict the future.
And this can help organizations make a lot of money or even help save the world by improving peoples their lives.

The warmer/colder clue in this is a statistical measure of distance called "RMSE" (Root Means Square Error).
This is a measure of the error (how bad) your model is.
It tells you how many predictions you got wrong and by how much.
A bunch of bad predictions and you get a really large RMSE.
A bunch of good predictions and you might get an RMSE close to zero.
RMSE is never negative because your machine learning model will never be better than perfect.
And your goal is to **minimize** this score to get as close to zero error as possible.

## RMSE (your score)

RMSE is almost the same formula you learned to compute in grade school with the Pythagorean theorem, add the square of the sides together and then take the square root.
Only RMSE is for a triangle with as many "sides" as you like.
The distance from each of your data points to the prediction line is a "side" of the triangle.
So RMSE works with more than just triangles, it works for long lists of numbers called arrays or vectors.
Don't worry, the game we've built will calculate RMSE for you by adding up all the errors in the predictions of your model (a red line on a scatter plot) and the true values (orange points on a scatter plot).
But if you're curious here's the formula for Root Mean Square Error in python:

```python
import numpy as np
error = predictions - true_values
rmse = np.sqrt(np.mean(error**2))
```

So this treasure map has the gold coins scattered around on a long line but it doesn't tell you where the line is.
You're only option is to tell a robot to walk in a line and report back what the total RMSE was (the distance to all the hidden coins along that line).
Once you've gotten the RMSE as low as possible, you could tell the robot to dig a trench along the line and hopefully he will find a bunch of coins.
That's why you want your predictions of where the coins are (the red line in the plot) to be as close to the coins (black dots) as possible.

But don't expect the graphic to update as you change the slope or intercept.
That's your job, to estimate or guess what the slope and intercept need to be for the line to be close to the coins.
In fact you don't need the plot at all.
Just like you close your eyes and play the Marco Polo or "hotter/colder" game, you only need someone to tell you how close you are to the right answers for slope and intercept.
And that's exactly what the RMSE value tells you.
That will update each time you select [submit answer].

#### Side note
If you want to improve the game you can help us add an estimate for the number of coins gathered up by the trench-digging treasure hunting robot.
the code is on gitlab: [tangibleai/proai_playground_backend](https://gitlab.com/tangibleai/proai_playground_backend) and [tangibleai/proai_playground_frontend](https://gitlab.com/tangibleai/proai_playground_frontend)

## Instructions

Fortunately this drunken sailor didn't wander too far from a straight line as he "planted" the gold coins (or maybe it's apple seeds and you are trying to put down an irrigation pipe).

You can think of it as a measure of distance from the treasure chest.
On a treasure map you might see a note saying to walk 2 paces (meters) North and six paces East from the base of a tall coconut tree.
I forward then 
Sign up for an account on our machine learning game: [play.qary.ai](https://play.qary.ai/) and complete at least 3 "games":

1. Click the [Start Game] button
2. Adjust the Slope and/or Intercept values in an attempt to improve the fit of the line to the points in the graph and reduce the RMSE value (error)
3. Click [Submit Answer]
4. Check the Answers Table for your submitted answer and do some math with pencil and paper to help you think of better guesses for the Slope and Intercept.
5. Repeat steps 2 through 4 until you can't improve the RMSE value anymore
6. Go back to step 1 two more times and see if you can get to the best RMSE value faster each time, using fewer clicks of the [Submit Answer] button.

When you are done, message me on a public Discord channel titled "Machine Learning" in the Python User Group Discord server.
I will check your answers and give you a score between 0 and 100.
If you get within 10% of the best possible RMSE value in less than 10 tries on at least one of your "games" you will receive a passing grade of 70%.
To achieve a grade of 100% you will have to demonstrate that you thoughtfully utilized math to compute most of your slopes and intercept guesses for all of your games. And you will need to achieve within 2% of the best possible RMSE on all of your games (on average).
