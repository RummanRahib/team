Configure an Ubuntu server for data science with a team

Most of this will apply, whether or not you use Digital Ocean to host your VMs:


### Goals
* user can ssh into an Ubuntu server
* user has access to shared Anaconda3 app: `conda --help`
* shared conda environments: `conda activate shared-environment-name`
* admin can attach/detach block storage "volume" to any Droplet 
* users can run all conda commands without sudo
* users can clone their repositories from GitLab (Allow)

```
sudo adduser somebody
sudo usermod -aG datascience somebody
sudo chown -R somebody:datascience /midata
sudo chmod -R a+x /midata
sudo chmod -R a+r /midata
sudo chmod -R o-r /midata
```


#### ssh somebody
```console
ln /midata ~/Dropbox
ln -s /midata ~/Dropbox
cd Dropbox
ls -al
exit
conda --help
/midata/anaconda/bin/conda init
/midata/anaconda3/bin/conda init
exit
conda
cd ~/
mkdir code
cd code
git clone git@gitlab.com:tangibleai/$PROJECT_NAME
exit
cd code
git clone git@gitlab.com:tangibleai/$PROJECT_NAME
ls -al
cd $PROJECT_NAME/
git checkout master
```

Make sure the project has a list of requirements/dependencies somwhere.

```
scp requirements.txt somebody@$SERVERIP:code/$PROJECT_NAME/
scp environment.yml ~/code/$PROJECT_NAME/
```

Back on the new server

```
conda create -n $PROJECT_NAME 'python>3.7,<3.8'
conda activate $PROJECT_NAME
pip install -r requirements.txt
# won't work for other users but should work for you
# git clone git@gitlab.com:tangibleai/qary
cd ~/code/$PROJECT_NAME/
pip install -r requirements.txt
nano ~/.bashrc  # HISTORY settings
```

