# Get Social

## Connection

The most important professional skill you will develop is the ability to build cooperative, mutually supportive relationships -- lasting connections.
Here are some communities and social networks where Tangible AI interns and team members connect.

The Tangible AI team is active at the highlighted links:

- **Tangible AI Social Networks**
  - **[qary Discord server](https://discord.gg/ENStXhfqRQ)**
  - [mastodon.social/@mdyshel](https://mastodon.social/@mdyshel), [mstdn.social/@rochdikhalid](https://mstdn.social/@rochdikhalid), [mstdn.social/@hobs](https://mstdn.social/@hobs)
  - [SubStack by Greg](https://chatbotdesign.substack.com/)
- **Tangible AI Open Source Software** on GitLab
  - [qary](https://gitlab.com/tangibleai/qary)
  - [nudger](https://gitlab.com/tangibleai/nudger)
  - [team resources](https://gitlab.com/tangibleai/team)
  - [Natural Language Processing in Action book examples](https://gitlab.com/tangibleai/nlpia2)
- **Qary**
  - [docs.qary.ai](https://docs.qary.ai)
  - [qary.ai](https://qary.ai)
- **San Diego Python User Group**:
  - **[SD Python Discord server](https://discord.gg/TWZ99B6nkU)**
  - [SanDiegoPython.org Home Page](https://www.sandiegopython.org/) 
  - [Piped/@SanDiegoPython - YouTube alternative](https://piped.kavin.rocks/channel/UCXU-oZwaHnoYUhja_yrrrGg) 
- **San Diego Machine Learning**:
  - [Meetup.com page](https://www.meetup.com/san-diego-machine-learning/)
  - [Slack workspace](https://join.slack.com/t/sdmachinelearning/shared_invite/zt-1jb9kcxdy-eOrTly_bj0GLCug2P0Mpeg) (Slack links expire, use your social and web surfing skill to find fresh ones)

## Support others

One of the fastest ways to learn and grow is to help others.
There are people at each of these communities who don't know things that you know or don't have some skill or resource that you have.
Seek out those people and help them.
Be someone's Superhero.
Some of your most supportive and valuable friends may turn out to be those people you helped early on.

And even if you can't find a way to teach someone something, you can always find ways to *support* others.
You can boost people on Mastodon or Substack servers by replying to their posts or giving them a "Like" or Emoji.
And follow people on Linked In, Mastodon, or Substack if they are posting things supporting others and talking about things that interest you.

## Dopamine discipline

Pay attention to your dopamine levels -- your mood and excitement.
Don't get dragged into conversations or threads that feel like echo chambers confirming your own beliefs.
You should feel the pleasing dopamine "hit" in your brain as a sense of "I told you so!" or "I was right all along."
Even worse is when you find yourself venting your anger and spite among people that you feel are working against you. 
