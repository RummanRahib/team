The fastest way to learn about anything deeply is to play with it.


## Play (Mess) with Bots

If you want to learn how GPT-3 or other chatbots work... mess with them, manipulate them, and try to get them to do unexpected things.
But wait isn't that anti-social?
It's actually prosocial to troll a bot.
Most bots are designed to manipulate you.
And no chatbot yet has been designed with a sense of morality or conscience.
And it may be impossible.

So practicing your bot resistance techniques is a necessary skill in the modern world.
A friend at a local startup was able to get the Mitsuku bot (rebranded Kuki.ai) to reveal much of its source code.
He used profanity and insulted it.
I was able to treat it politely and achieve the same result.
And I was even able to discover how it maintains "context" between turns and even between sessions: [mitsuku-chat-log/...-profile-context.png](mitsuku-chat-log/mitsuku-kuki-ai-99-profile-context.png)
Can you?

See how much information you can get out of it about how it works.
Remember it is likely based on machine learning models for NLU and intent recognition.
So use what you know about how these systems work to get it to reveal different things about how it works.
If you run out of ideas, check out the screenshots in this folder to see if some of my tricks work for you.

Apparently the programmer of Mitusku (rebranded as kuki.ai) had some heated debates with himself that he recorded in the training set.

## Play with regression

[Play.qary.ai](https://play.qary.ai) is like Golf, you want to reach the bottom of the hole (minimum RMSE) with as few shots as possible.

Set up your account at [play.qary.ai/signup](https://play.qary.ai/signup) and then sign in play.qary.ai/login](https://play.qary.ai/login).
Don't worry we erase the database every time we deploy.
We will only use your "shots" to evaluate your application for an internship at Tangible AI.
So if you don't apply, we'll probably never even see it.

### A round of regression

If you're like me, you may want to just poke around and see if you can figure out how it all works.



### Advanced

If you get really curious you can find the source code at [gitlab.com/tangibleai/proai_playground_frontend](gitlab.com/tangibleai/proai_playground_frontend) and [gitlab.com/tangibleai/proai_playground_backend](gitlab.com/tangibleai/proai_playground_backend).
