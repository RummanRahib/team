# Chapter 1: Applications and Tokenization

# Submitting your answers

Create a 'chapter1_submission.py' file and run the chapter1_answers.py python script on your answers.

Chapter 1 is about applications for NLP(NLU and NLG).
And it shows you how the first part of any NLP pipeline is breaking the text into into tokens.
And tokens are whatever parts of the string you want them to be(depends on your problem).
If you want your NLP pipeline to understand or generatoe words like names of people or businesses you probably want your tokens to be characters rather than words.

```python
surname = 'Khalid'
surname_tokens = []
for character in text:
    surname_tokens.append(character)
tokens
# ['K', 'h', 'a', 'l', 'i', 'd']
```

1. Create a variable `surname` that contains the text for your surname (last name). Create another variable called `name_tokens` that contains a sequence (`list`) of characters, one for each letter in your last name? What are the character-level tokens for your surname (last name)?

2. Create a function called `char_level_tokenizer` that takes as an input any text (`str`) and splits the text into characters just like you did with your name. What is the character-level token sequence for the business name "Tangible AI."

3. Can you shorten the code for your tokenizer without changing any variable names so that it all fits on one line?

<!-- # Answer
```
def char_level_tokenizer(text)
    return [character for character in text]

char_level_tokenizer('Khalid')
# ['K', 'h', 'a', 'l', 'i', 'd']
```
 -->

4. Create a keyword function called `char_level_tokenizer` that splits any string into characters just like you did with your name. What is the character-level token sequence for the business name "Tangible AI."

<!-- # Answer
```
tokens = [character for character in text]
tokens
# ['K', 'h', 'a', 'l', 'i', 'd']
```
 -->

Your f
tokenization and regular expressions
