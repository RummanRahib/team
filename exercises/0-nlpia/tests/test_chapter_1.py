# chapter1_check.py
from chapter1_submission import *

NUM_CORRECT = 0
NUM_ATTEMPTS = 0
NUM_ERRORS = 0

edge_case_other = [None]

edge_case_floats = [
    -float('inf'), float('nan'), 1 / 1e301, -1 / 1e300, -1, 0, 0.0, 1, 1.0, 2, 3, 100, 3.14, float('inf'),
]

edge_case_strs = ['inf', 'nan', '']


def test_answer_1():
    global NUM_CORRECT, NUM_ATTEMPTS, NUM_ERRORS
    NUM_ATTEMPTS += 1
    try:
        text = globals()['text']
        if text and (list(text) == globals()['tokens']):
            print('Correct!')
            NUM_CORRECT += 1
    except Exception as e:
        print(e)
        NUM_ERRORS += 1

    try:
        text = globals()['text']
        list(text) == globals()['tokens']
        print('Correct!')
        NUM_CORRECT += 1
    except Exception as e:
        print(e)
