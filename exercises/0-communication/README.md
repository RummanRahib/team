# Public speaking and written communication tips

## TOC

Three sections for each of 3 different "courses" in communication.

- [Springboard](#springboard)
- [Quickbase](#quickbase)
- [Engineering Management Institute](#engineering-management-institute)

## Springboard

Industry and research insights into effective communication.

Reference: [Springboard Mentor Training on Communication](https://www.springboard.com/learning-paths/mastering-mentoring/learn/116/)


### 1. Know your Mentee's Communication Style

-   How does your mentee like to receive information? Ask your mentee how they prefer to communicate. Your mentee's communication style will effect how you communicate during the weekly course call.

### 2. Know Your Communication Style

-   How do you like to communicate? How do you deliver information? Reflect on this and how you might adjust your style as needed when interacting with different mentees. 

### 3. Encourage Dialogue

-   Some students may need you to drive the conversation. You can lead by asking them questions, highlight what they did well, what they can improve on, and what they plan to do next

### 4. Balance Listening and Talking

-   It is always a balancing act between listening and talking. Each mentee will have different needs. Balance between listening and talking accordingly.

### 5. Use Language with Intention

-   While sharing insights from your experience is basic to mentorship also demonstrate empathy with your language and clearly involve your mentee and listen to their experience.

### 6. Establish a Foundation of Trust

-   Authenticity is key to long-term trust. Work to build in the elements of rigorous logic, authenticity and empathy into your interactions and communication.

### 7. Limit Calls to the Weekly Timeframe

### 8. Be Empathetic

-   Work to understand your mentee's context and communication and let them know they have been heard. 

### 9. Be Familiar with the Curriculum

-   Communication is stronger when the mentor and mentee share the same language and information about the course. 

### 10. Teamwork

-   If you are having any communication challenges, notify the Mentor Operations Team. 

----

## Quickbase

Reference: [Quickbase: Ways to Clearly Communicate Complex Information](https://www.quickbase.com/blog/6-ways-to-clearly-communicate-complex-information)

### 1. Be concise

As Albert Einstein once said, "If you can't explain it simply, you don't understand it well enough." Try to keep your opening sentence to less than 50 words. After that, use the "Twitter test" and try to reduce each important point down to 140 characters. You may not hit that number exactly, but it will force you to think of boiling the information down to the bare bones.

Concision is a skill that doesn't only help you communicate complex information, but it can help lead you to [solutions for complex problems](https://www.quickbase.com/solutions). Being able to target, identify, and express a pain point or issue is essential to solving it in a way that is ultimately successful.

### 2. Tell stories

Scientists and other technical experts often begin a report with data and statistics. While these can be powerful for engaging the audience and adding credibility, relying too heavily on numbers can quickly bore your listeners. By thinking of how the information can be crafted into a narrative, the audience is immediately re-engaged.

"\[A\]s a storyteller, you want to position the problems in the foreground and then show how you've overcome them. When you tell the story of your struggles against real antagonists, your audience sees you as an exciting, dynamic person," says Robert McKee, a creative writing instructor known for his popular "Story Seminar."

Learning to express information as a story is a valuable skill because it provides important context such as what the conflict is, who is involved, and when and where it occurs. Providing this basic context does so much more than just engage your audience – it can give you a roadmap to a solution.

### 3. Making it visually enticing

New research in the Journal of Consumer Psychology finds that visual metaphors – such as an illuminated light bulb to suggest new ideas – can prompt participants to have better insights than those who are shown no image. Consider sites like Flickr, Creative Commons and [Compfight.com](http://compfight.com/) to find images to use in a presentation.

If you're looking for a more compelling way to present data to your team or boss, utilizing a tool with custom dashboards and different ways to visualize data will go a long way. Quick Base's dashboard reporting tools allow users to visualize data in many forms: summary reports, charts, maps, calendars, and timelines. Users can create unlimited reports and easily slice, dice, and organize the most important and complex information.

### 4. Using metaphors and analogies

When Jobs first tried to explain to others what a personal computer was and how it would work, he became frustrated when others didn't understand. But he learned to use metaphors and analogies to communicate complex messages. For example, Jobs described IBM in 1984 as "Big Brother come to life," bringing the idea to a famous television commercial pitting the Mac against IBM. Jobs also once described a computer as "the most remarkable tool that we have every come up with. It's the equivalent of a bicycle for our minds."

Analogies have the power to "make it click" in the mind of your audience by drawing a comparison between something unknown and something familiar. This makes new information less daunting and more approachable – and thus, more actionable.

### 5. Continually asking "so what?"

Challenge each of your key points to ensure that you're relating it to the listeners and their lives. Again, Jobs was a pro at doing deep research so that he understood a product thoroughly. He could put it in terms that answered anyone's "so what?" whether the person was 6 years old or 60 years old. In 2007, when he introduced the iPhone, he said: "We've designed something wonderful for your hand." Who can't grasp that?

If you want people to care about what you have to say, you need to give them a reason. You need to tell them _why_. You need to understand the value this information provides not just the business in general, but how it can improve people's lives and jobs.

### 6. Using Technology that Helps People Understand

Finding good ways to present information and [convey complex messages](https://www.quickbase.com/solutions/process-improvement-software) clearly can be hard, but you don't have to do it completely on your own.

Utilizing technology like [low-code applications](https://www.quickbase.com/business-application-platform/what-is-low-code) can help more people understand your message. Low-code business applications can integrate with the programs and software your business relies on to identify key issues that may be holding your business operations back. It can also be used to [automate essential workflows](https://www.quickbase.com/solutions) and track progress through complicated processes, so you are always able to answer the _who, what, when_, and _where_ of any issue that arises. These applications also support dashboards that display important data in tables, charts, and graphs that can be updated in real-time. Most importantly, implementing low-code applications into the processes you rely on day-to-day can quickly drive real business value – giving you the ability to communicate the _why change_ part of your message in a way that resonates with your audience.

----

## Engineering Management Institute

- [Engineering Management Institute: Communicating Clearly for Engineers](https://engineeringmanagementinstitute.org/communication-engineers-important/)

### What are the keys to clear communication?

What are the keys to clear communication?

Proper Speech

When speaking, be clear and concise. Speak on important matters directly and do not allow the listeners minds wander. Make sure that you are certain they understand what you are communicating and further explain any points necessary. Do not expect someone to "know" what you are saying. Do not "ASS"ume.
Listening

A key to verbal communication for engineers is listening. Not just waiting to speak. If you want to really listen to someone, make mental notes of key points when someone is speaking to you. What you would like to say should come second to understanding. When you have a chance to speak, you can respond to the most vital issues. When others are speaking, focus on the exact words people are saying. This will help you comprehend more information and have a more successful conversation.

If you are confused, repeat what you think they said and ask if that is correct. This inspires the speaker to clarify their needs, which will help you to understand.
Consistent Communication

Great leaders practice consistent communication. Each one of us has a different approach to how you portray a vision. Find your own voice. Then practice consistency. The consistency of your approach will help your family, friends, and coworkers comprehend your information more easily.
Patience

During your communications give others time to communicate. Stay focused on what they are trying to communicate and stay open to assisting with their issues. Communication lines tend to break down where impatience gets in the way of the conversation. Every conversation you are involved in is important!
Following Up

When someone communicates an issue to you, your main priority should be to solve their problem. Following up on a problem is the only way to convince people you have listened to them and that their problems or issues are important to you as well.

Practicing consistent follow-up will leave the impression that you are involved in the bigger picture. People will learn that you are open to future communications. This makes you loyal! Others will be confident that they can rely on you.
Business Communication Success

Both written and verbal communication skills are of the utmost importance in business, especially in engineering. Communication skills boost you or your teams' performance because they provide clear information and expectations to help manage and deliver excellent work.

    Leave open communication lines to those who need you. You can prevent small issues from turning into larger ones, by leaving open lines of communication.
    Make rules. Make sure people in your business understand what they should and should not be saying.
    Create an open-door policy. Make sure employees can talk with management at any time.
    Give feedback. Be genuinely interested in issues, comments, and concerns.
    Communicate the future. Update employees regularly about company goals and vision.

#### Proper Speech

When speaking, be clear and concise. Speak on important matters directly and do not allow the listeners minds wander. Make sure that you are certain they understand what you are communicating and further explain any points necessary. Do not expect someone to "know" what you are saying. Do not "ASS"ume.

#### Listening

A key to verbal communication for engineers is listening. Not just waiting to speak. If you want to really listen to someone, make mental notes of key points when someone is speaking to you. What you would like to say should come second to understanding. When you have a chance to speak, you can respond to the most vital issues. When others are speaking, focus on the exact words people are saying. This will help you comprehend more information and have a more successful conversation.

If you are confused, repeat what you think they said and ask if that is correct. This inspires the speaker to clarify their needs, which will help you to understand.

#### Consistent Communication

**Great leaders practice consistent communication.** Each one of us has a different approach to how you portray a vision. Find your own voice. Then practice consistency. The consistency of your approach will help your family, friends, and coworkers comprehend your information more easily.

#### Patience

During your communications give others time to communicate. Stay focused on what they are trying to communicate and stay open to assisting with their issues. Communication lines tend to break down where impatience gets in the way of the conversation. Every conversation you are involved in is important!

#### Following Up

When someone communicates an issue to you, your main priority should be to solve their problem. Following up on a problem is the only way to convince people you have listened to them and that their problems or issues are important to you as well.

Practicing consistent follow-up will leave the impression that you are involved in the bigger picture. People will learn that you are open to future communications. This makes you loyal! Others will be confident that they can rely on you.

#### Business Communication Success

Both written and verbal communication skills are of the utmost importance in business, especially in engineering. Communication skills boost you or your teams' performance because they provide clear information and expectations to help manage and deliver excellent work.

1.  **Leave open communication lines to those who need you.** You can prevent small issues from turning into larger ones, by leaving open lines of communication.
2.  **Make rules.** Make sure people in your business understand what they should and should not be saying.
3.  **Create an open-door policy.** Make sure employees can talk with management at any time.
4.  **Give feedback.** Be genuinely interested in issues, comments, and concerns.
5.  **Communicate the future.** Update employees regularly about company goals and vision.

----
