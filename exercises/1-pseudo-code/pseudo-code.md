# Pseudocode

## What Pseudocode is?

Pseudocode is an informal way to write a program in a natural language that is easy to understand. It consists of sequence of steps and actions that help you and non-programmers understand the logic of the program without thinking about the core syntax and principles of a programming language.

## Benefits of writing a Pseudocode

* Pseudocode represents a very simple method that you can get started with to develop a software program. It connects your brain and your computer's code executor to allow you solve your coding problems in form of instructions without diving into the technical requirements. 

* Pseudocode describes how the algorithm of a certain program should work to meet the expectations, so the non-programmers and people with less-technical background can easily understand the algorithm.

* Pseudocode helps you improve your problem solving skills and create a good quality code. Since the Pseudocode is a human-readable format, it is more simple to revise and find bugs in pseudocode rather than testing, and debuging the actual code.

## Pseudocode elements

Several elements of programming logic are described in pseudocode, the most common elements in pseudocode are as follows: 

* SEQUENCE: indicates a series of linear tasks that are completed one after another. 

* IF-THEN-ELSE: describes what will happen if a specific event occurs or doesn't occur. 

* CASE: describes what will happen when each of the multiple events occur

* FOR: loop that explains the action should be applied to every item in a dataset

* REPEAT-UNTIL: loop that runs an action until a particular event occurs

* WHILE: loop that runs an action when a particular state is true

## Examples of Pseudocode

This is basic example of a Pseudocode to create a program that add two numbers together and display the result: <br/>

**Start program**

* Enter two numbers x, y
* Add the numbers together
* Print sum

**End program**<br/>

Another example of a Pseudocode to determine if a number is even or odd:<br/>

**Start program**

* Read num

* IF num is divisible by 2

    * print even

* ELSE 

    * print odd

* ENDIF

**End program**<br/>

Another example of a Pseudocode to create a program that greets the new users:<br/>

**Start program**

* Read users

* FOR users

    * Print welcome to our community

* ENDFOR

**End program**<br/>

Another example of a Pseudocode to create a program that filer the negative numbers:<br/>

**Start program**

* Read nums

* Read neg_numbers

* FOR nums

    * IF nums greater than 0

        * Add nums to neg_nums

    * ENDIF

* ENDFOR

* Print neg_nums

**End program**

## Disadvantages

* It could be difficult for team members to understand each person's preferences or writing style. 

* Creating a Pseudocode needs time and planning, which could delay writing the actual code. 

## Tips on how to write a good Pseudocode

Before writing a Pseudocode, remember the following basic guidelines:

* Start by writing down the purpose 

* Use simple words to describe the problem

* Remember to begin your program with "Start program" and stop it with "End program"  

* Each line should only include one task or action

* Capitalize the key words like Display, Print, etc

* Indent the code when using multiple Pseudocode elements

* Show the Pseudocode element is completed by adding END at the beginnig of each element (ENDIF, ENDFOR, etc)

## Useful resources

* [What is Pseudocode? How to Use Pseudocode to Solve Coding Problems](https://www.freecodecamp.org/news/what-is-pseudocode-in-programming/)

* [How To Write Pseudocode (Definition, Components and Pros)](https://www.indeed.com/career-advice/career-development/pseudocode)

* [How to Write Pseudocode](https://www.wikihow.com/Write-Pseudocode)






