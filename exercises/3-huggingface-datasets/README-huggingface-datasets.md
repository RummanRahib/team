# Huggingface datasets

A great way to support the open source AI research community is to contribute code, models, documentation, or **datasets** to the Huggingface community.
And they will support you by allowing you to host your model on their servers as a web application using `gradio` or `streamlit` or even raw HTML5 (javascript).
When you create a dataset on huggingface you get the hosting of your dataset for free, so you can use it anywhere you have an Internet connection.

The Huggingface community includes:

* data cards (dataset versioning and hosting)
* spaces (web applications for ML models)
* models
* cards (documentation)
* organizations

This exercise will focus on creating and curating datasets, an underappreciated way to reduce bias in the AI models being trained and deployed by technology companies.

## Quickstart

Here's a way to get started with Huggingface datasets.
But feel free to skip to the advanced exercise and create your own dataset if you have already mastered the art of combining dataframes into a single CSV.

1. Run [this jupyter notebook](https://gitlab.com/tangibleai/experiments/-/blob/main/huggingface_datasets/combine_pyarrow_dataframes_in_csv.ipynb) to create a paraphrases dataset
2. Add your own paraphrases of these sentences or correct the grammar or ambiguity in any of the existing paraphrases.
3. Turn some of the quora questions and their paraphrases into statements.
4. Upload your changes to HuggingFace spaces

## Advanced

1. browse the huggingface datasets and problem types
2. Find a dataset or problem type (called "task" on Huggingface) that does not have many labeled examples
3. Convert some portion of the data to a dataframe.
4. Do `df.describe(include='all')` and `df.info()` to understand the dataset better. Pay close attention to class biases and new class/category labels you might want to add.
5. Read through a `df.sample(100)` of a test set or training set to understand what you are working with and see if you can find any errors or improvement opportunities. 
6. Think about ways you can use datasets for other problems to create new examples for your "underserved" dataset or problem type (task).
7. Think about programmatic ways to augment an existing dataset. Search for "robust NLP by Robin Jia" to get some ideas if you're working on an NLP dataset. "robust AI/ML/DL" might work for other kinds of data.
8. Think about social media platforms like ActivityPub fediverse microblogs where you might be able to find some particularly rare kinds of data from prosocial people and diverse cultures.
9. Go to town! Create your own dataset from what you've found on social media, edits of combinations of existing datasets.
10. Use some of your examples to "fool" some existing SOTA (state of the art) models by doing (`model.predict`).
11. Upload your dataset to Huggingface
12. Write up a blog post about what you found.
13. Finetune or retrain a model from paperswithcode.com or huggingface, looking for improvement against benchmark datasets, greater robustness of your model, and reduced bias of your model's predictions.



## References:

- [Meijke's tutorial on robust NLP](https://www.youtube.com/watch?v=PKlYrtyZsrU)
- [June 23 2022 SDPUG lightning talk about huggingface spaces](https://www.youtube.com/channel/UCXU-oZwaHnoYUhja_yrrrGg/videos?view=0&sort=dd&flow=grid)
- [huggingface docs](https://huggingface.co/docs/datasets/share)
- [example jupyter notebook](https://gitlab.com/tangibleai/experiments/-/blob/main/huggingface_datasets/combine_pyarrow_dataframes_in_csv.ipynb)
