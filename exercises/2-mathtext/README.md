# Parse English Number Words in Text

Would you like to help us build a Math Quiz Bot for K-12 students in Africa?

You have one week to write a Python function to help us process student chat messages:

#### *`mathtext.py`*
```
def text2int(text):
    """ Convert an English str containing number words into an int

    >>> text2int("nine")
    9
    >>> text2int("forty two")
    42
    >>> text2int("1 2 three")
    123
    """
    pass
```

## Instructions

1. Fork this repository
2. `cp -r team/exercises/2-mathtext $YOUR_NAME`
3. Add some test cases to your directory
4. Complete the Python code snippet in your directory
5. Push your code and data to your fork on GitLab
6. Create a merge request to this repository
7. Reply to the UpWork "qualifying question" with a link to your Merge Request

The top submission will be rewarded $100.
All other valid submissions will be paid $10.

Your submissions will be scored and ranked based on:

### 1. Speed

Only the first 10 valid submissions will be considered.
No submissions will be accepted after Dec 23 22:00 UTC.

### 2. Test cases (50% of your score)

Using the example test cases in the docstring above, can you think of some larger numbers or edge cases?
What English words would a teenager in Africa use when chatting with a math quiz bot?
Add your examples to one of the following files:

- [`test_text2int.csv`](test_text2int.csv)
- [`test_text2int.txt`](test_text2int.txt)
- [`test_text2int.yaml`](test_text2int.yaml)

Your submission will be scored based on the number of your test cases that the other submissions fail to match correctly but your code does match correctly.

### 3. Code correctness (50% of your score)

1. Fork this repository
2. Clone your fork.
3. Copy the enclosed `mathtext/` directory to a new directory named for yourself (or your GitLab username): e.g. `cp -r team/exercises/2-mathtext/mathtext exercises/2-mathtext/hobs`.
4. You may install and use any of the Python packages listed in `2-mathtext/requirements.txt` 
5. Finish writing the function definition in `2-mathtext/$YOUR_NAME/mathtext.py` so that it works for the 3 examples in the docstring (9, 42, 123).
6. You may want to improve your function definition to work with some of the tests in the text files here (`mathtext/test_*.*`)
7. You may want to improve your function to be able to handle the new test examples you created earlier.

## Optional

For your own growth, feel free to submit an answer to the popular Stack Overflow question [convert-number-words-to-integers](https://stackoverflow.com/q/493174/623735)
