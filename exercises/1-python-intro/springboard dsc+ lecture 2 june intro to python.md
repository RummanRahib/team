
10 quizzes

## help

Name at least 5 ways to get help without using the internet.

```python
help()
?
??
print()
```

tab-completion

## built in functions

Name at least 
```python
str()
repr()
len()
print()
```

## built-in numerical types

Name two python data types for storing a single number.

int
float


## everything is an object

```
dict.get()
duct.update()
__str__
__repr__
```

## built-in types

int
float

## builtin container for text

str

What does this output:
```python
text = "Hello"
text
```

```python
text[1]
```

```
"Hello"[1]
```

## built-in container types

```
str
list
set
dict
```


nums = [1, 2, 4]
nums

nums = nums.append(8)
nums

nums = nums.extend([16, 32])
x 

 = list(''abc")

## external containers for numerical data

Name 3 containers for numerical data. Hint, where do we usually store data for data science?

```
pandas.DataFrame
pandas.Series
numpy.array
```

What does this output:


```
import numpy
x = numpy.array([1,2,4,8])
x
```

```
import pandas
s = pandas.Series(range(4))
s + pandas.Series(range(0, -4, -1))
```


## modules

Name 4 python packages or modules (imports) that you use in data science.


math
Pandas
Sci
create your own module
