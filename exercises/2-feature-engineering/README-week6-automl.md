This week you're going to learn about the different kinds of data you can work with in data science, such as categorical, ordinal, and numerical data.
The first step in any Data Science project is to recognize what kind of data you're working with.
And you're going to write a function so that your python code can do that tedious work.

Here's this week's exercise on the gitlab team repository: [proai.org/2021q3wk6](https://proai.org/2021q3wk6). 
