# intern-challenge-01-skills-checklist.md

Your exercise this week is a self assessment.
This will help us craft an internship with your learning needs.

Try not to use the links here, so you can learn how to find repositories and files on gitlab.com.

- Create a gitlab account
- Fork the [tangibleai/team](http://gitlab.com/tangibleai/team) repository
- Find the [`blank.md`](http://gitlab.com/tangibleai/team/-/blob/master/interns/skills-checklist/blank.md) file in the `interns/skills-checklist` directory
- Click the [Edit] or [Raw] button in the gitlab GUI
- Select all of the checklist text and copy it
- Create a new file in teh `interns/skills-checklist` directory.
- Check off the skills that you think you have mastered
- Commit your file to your fork of the repo

## Bonus

- Create a `[Merge Request]` (MR) from your `[Fork]` to Tangible AI version of the `team/` repository.
- Assign a reviewer to your MR: `@hobs`, `@dwayne.negron1`, `@winston.oakley`, `@hmseyoum`, or `@billy-horn`
