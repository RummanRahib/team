# intern-challenge-15--deploying-a-django-api-using-docker

John May created a [Django API repo](gitlab.com/tangibleai/django-api) that we use to serve up a recommendation engine for nonprofits around the world.
They typically use it within a chatbot to recommend video content based on the history of things the user has talked about with the chatbot.

You can reuse it to "productize" almost any python function you've created in for your capstone project.
Here's how you can clone and deploy this project for yourself.
You can then customize it to serve up whatever REST API you would like qary or any other app to have access to.

# intern-challenge-15 Deploy a Django API using Docker

Are you ready to share your prosocial machine learning model with the world?
You can set up a REST API, just like OpenAI (only more open).
A REST API allows others to use your model in their apps, or for you to build a mobile app or web app or even a whole company, like [Vish has done](http://maurya-ai.com).

## Example

Here's our [Django API app](http://gitlab.com/tangibleai/django-api).
It's a Dockerized, reusable Django app that we use to deploy a recommendation engine API.
Some nonprofits like to use chatbots that recommend content (usually videos on YouTube, Vimeo, or Facebook) for their beneficiaries.

## Deployment

You can use the same steps that our CI-CD pipeline uses, to deploy the app to your own server.
In gitlab you define your CI-CD (auto-deployment) process in a .gitlab-ci.yml file.
Here's our [`.gitlab-ci.yml`](https://gitlab.com/tangibleai/django-api/-/blob/master/.gitlab-ci.yml)

## DEBUGGING

```bash
DC_FILENAME = docker-compose.prod.yml
docker-compose -f $DC_FILENAME logs -f
```

The first `-f` specifies the filename, in case it's not the default (`docker-compose.yml`).
The second `-f` is for the `logs` command to tell it to "follow" the log files live as the errors/warnings stream by while you interact with the API.

## Resources

- [Cory Schafer's Django Tutorial](https://coreyms.com/development/python/python-django-tutorial-full-featured-web-app-part-13-using-aws-s3-for-file-uploads)
- [Django Channels - Realtime Log of Users Online](https://realpython.com/getting-started-with-django-channels/)
- [Django Channels + Angular - Taxi Routing App](https://testdriven.io/courses/real-time-app-with-django-channels-and-angular/part-one-authentication/)
- [Django Channels - Communicate from DRF REST with Websockets/Channels](https://dev.to/buurak/django-rest-framework-websocket-3pb6)
