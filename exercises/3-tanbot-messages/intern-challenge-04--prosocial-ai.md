# Elevate Prosocial AI

Let's take a break from analytical, left brain thinking.
Time to exercise your right brain with some big picture thinking tools.
You're going to give your prosocial self a workout this week by researching and understanding what makes a statement, idea, organization more prosocial.

Check out [this exercise](../1-prosocial-ai/)
