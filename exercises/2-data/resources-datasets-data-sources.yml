# Datasets for machine learning and NLP
#
# SEE ALSO:
#   qary repository: src/qary/data/datasets.yml
#   nlpia2 repository: src/nlpia2/data/datasets.yml or resources-datsets-data-source.yml
#   team repository: data/exercises/2-data/resources-datasets-data-sources.yml
#
# data science datasets and data sources data aggregators data indexes
# mirrored in the nlpia2 repository at .nlpia2-data/resources-dataset-data-sources.yml
# For machine learning you just want a *tall* tabular dataset, one that has many more rows than columns.
# You also would like to know a little something about kinds of records (rows) in the dataset.
# Is each row a person? a place? a product?
# And you would like to be able to imagine ways you might convert most of the values to numbers.
# Machine learning is just math.
# So you need to convert a table of strings and dates and addresses to numbers before you can get started.

# Look for a column that would be interesting to predict (the target variable) if you know all or some of the other columns (the feature variables).
# Don't pay attention to how others used the data for machine learning or statistical modeling.
# Your idea may be better.
# And it will certainly be more fun that what someone else dreamed up.
-
  name: Omdena
  description: Sign up for projects to gain access to data
- 
  name: fsu.edu CSV file format examples
  url: https://people.sc.fsu.edu/~jburkardt/data/csv/
  description: small CS course datasets by Jack Burkardt
- 
  name: Stanford GloVe word vectors
  url: https://nlp.stanford.edu/projects/glove/
- 
  name: pd.read_html on Wikipedia tables
  url: wikipedia.org)"
- 
  name: paperswithcode.com datasets
  url: https://paperswithcode.com/datasets
  description: state of the art benchmark
- 
  name: fsu.edu DS datasets
  url: https://people.sc.fsu.edu/~jburkardt/datasets/datasets.html
  description: small machine learning and computer science by Jack Burkardt
- 
  name: Google N-grams
  url: https://storage.googleapis.com/books/ngrams/books/datasetsv3.html
  description: "Billions of 1-5-grams from books (Gutenberg), lags 1 yr behind search engine (2020-02 data available in 2021-02)"
- 
  name: Kaggle.com
  url: kaggle.com
- 
  name: TREC datasets
  url: https://trec.nist.gov/data/qamain.html
  description: Text Retrieval conference benchmark datasets, including QA (reading comp
- 
  name: data.world
  url: data.world
  description: Tabular datasets (CSV) associated with published studies
- 
  name: Data.gov
  url: data.gov
  description: difficult to use and files in archaic formats
- 
  name: paperswithcode.com
  url: https://paperswithcode.com/datasets
  description:  benchmark datasets for academic research
- 
  name: NYC Data
  url: https://data.cityofnewyork.us/browse
- 
  name: programmableweb.com API search
  url: https://www.programmableweb.com/category/open-data/apis?category=20316
  tags: [api, open]
-
  name: github topic *datasets*
  url: https://github.com/topics/datasets
  tags: [meta, datasets]
-
  name: Awesome Datasets
  url: https://github.com/awesomedata/awesome-public-datasets
  tags: [meta, datasets]
-
  name: Lionbridge Machine Learning Datasets
  url: https://lionbridge.ai/datasets/
  tags: [meta, datasets, marketing, consultancy]
-
  name: Dimagi's top 10 open source datasets for NLP
  url: https://analyticsindiamag.com/10-nlp-open-source-datasets-to-start-your-first-nlp-project/
  tags: [open, prosocial, datasets, meta]
-
  title: Goodreads API
  url: https://www.goodreads.com/api
  description: natural language, book reviews, quotes and excerpts from books, information about authors 
  terms: don't index the data, link back to source of data, identify Goodreads as the source (but I don't think the terms apply to machine learning systems and may be illegal anti-competitive)
  tags: [nlp, corpora, quotes, natural language, English, text, books]
-
  title: Open Parallel Corpora for machine translation
  url: https://opus.nlpl.eu/index.php
  descriptoin: terrabytes of text in 50+ languages, aligned by sentence
- 
  title: "List of datasets for machine learning research"
  url: https://en.wikipedia.org/wiki/List_of_datasets_for_machine-learning_research
  description: list of datasets used in peer-reviewed research papers
-
  title: "Better Word Representations with Recursive Neural Networks for Morphology"
  url: http://www-nlp.stanford.edu/~lmthang/morphoNLM/rw.zip
  urls:
    - "https://nlp.stanford.edu/~lmthang/morphoNLM/cwCsmRNN.words"
    - "https://nlp.stanford.edu/~lmthang/morphoNLM/"
  description: |+
    [Better Word Representations with Recursive Neural Networks for Morphology](https://nlp.stanford.edu/~lmthang/morphoNLM/)
    #### `rw/rw.txt` (92.5 kB, 2034 lines): 
    ```text
    squishing squirt  5.88  7 7 6 1 4 6 6 7 2 4
    undated undatable 5.83  10  9 6 5 5 7 7 9 2 5
    circumvents beat  5.33  7 7 3 9 8 6 3 2 0 6
    circumvents ebb 3.25  7 4 6 4 2 0 0 3 6 0
    ```
    #### `rw/rw.txt` (92.5 kB): 
    ```text
    >>> words = open('cwCsmRNN.words').readlines()
    >>> len(words)
    138218
    >>> words[1000:1005]
    ['0kb', '0kbushel', '0keystart', '0kg', '0-kg']
    ```
-
  title: "Reinforcement Learning for Personalized Drug Discovery and Design for Complex Diseases: A Systems Pharmacology Perspective"
  description: Personalized Drug Discovery
  authors: [Ryan K. Tan, Yang Liu, Lei Xie]
  urls:
    - https://arxiv.org/abs/2201.08894
  tags: [paper, UCSD, DSDH, digital health, 2022, healthcare, academic]
-
  description: AlphFold Protein Structure (Proteome) Predictions
  urls: 
    - https://arxiv.org/abs/2201.09647
    - https://www.alphafold.ebi.ac.uk/download
    - https://www.alphafold.ebi.ac.uk/
  tags: [dsdh, paper, deepmind, meta, biology, alphafold, list, table, datasets, proteome, protein, structure]
-
  description: genomics and gis and other speciality datasets with juptyer notebooks
  url: https://github.com/markusschanta/awesome-jupyter#domain-specific-projects
-
  url: https://data.ca.gov/datastore/dump/240a9c60-1ec7-4d59-8db6-fceaadc17863?q=&sort=_id+asc&fields=county%2Ctodays_date%2Chospitalized_covid_confirmed_patients%2Chospitalized_suspected_covid_patients%2Chospitalized_covid_patients%2Call_hospital_beds%2Cicu_covid_confirmed_patients%2Cicu_suspected_covid_patients%2Cicu_available_beds&filters=%7B%7D&format=csv
  tags: [CA, gov, California, county, hospital, US, COVID, time series, UCSD, DSDH, healthcare, disease, epidemiology]
-
  url: https://data.cdc.gov/api/views/9mfq-cb36/rows.csv?accessType=DOWNLOAD
  tags: [gov, cdc, us, covid, time-series, UCSD, DSDH, healthcare, disease, epidemiology]
-
  url: https://github.com/datasets/covid-19
  tags: [time series, epidemiology]
-
  url: https://github.com/datasets/awesome-data
  description: polished frontend at https://datahub.io/collections
  tags:
    - Air Pollution data
    - Bibliographic data
    - Broadband data
    - Climate Change
    - Demographics (population)
    - Economic Data and Indicators
    - Education 
    - Football
    - GeoJSON
    - Health Care Data
    - Inflation
    - Linked Open Data
    - Logistics
    - Machine Learning / Statistical
    - Movies and TV
    - Open Corporates
    - Property Prices
    - Reference Data
    - Stock Market Data
    - War and Peace
    - Wealth, Income and Inequality
    - World Bank
    - YAGO
-
  url: paperswithcode.com
  description: high quality benchmark datasets for state of the art problems
-
  url: kaggle.com
  description: must sign up for an account, urls not publicly accessible outside kaggle
-
  url: data.world
  description: "community data is free and open source but hard to find"
-
  url: data.gov
  description: really poor dataset aggregator that favors advertisers and paywalls
-
  url: datasetsearch.google.com
  description: really poor dataset aggregator that favors advertisers and paywalls
-
  url: https://data.world/search?q=electric+vehicles
  description: data aggregator from data.gov etc
- 
  url: https://raw.githubusercontent.com/Davidelanz/nlp-contextual-meaning/master/dataset/situations_it.csv
  problem: foreign language intent classification
-
  url: https://raw.githubusercontent.com/subashgandyer/NLP/master/Python-NLP/train.csv
  problem: binary sentiment classification of tweets
  example: https://github.com/subashgandyer/NLP/blob/master/Python-NLP/TextProcessingusingPython.ipynb
-
  url: https://github.com/google-research-datasets/sentence-compression/tree/master/data
  problem: sentence compression, simplification, paraphrasing in English

# Happiness datasets
-
  problem: predicting country gross happiness (World Happiness Report) and GNH (Gross National Happiness), also the Easterlin paradox
  url: https://en.wikipedia.org/wiki/OECD_Better_Life_Index
-
  problem: predicting country gross happiness (World Happiness Report) and GNH (Gross National Happiness), also the Easterlin paradox
  url: https://www.kaggle.com/unsdsn/world-happiness
# GEC (Grammar Error Correction) Datasets
-
  problem: English GEC
  docs: https://www.cl.cam.ac.uk/research/nl/bea2019st/
  description: _FCE v2.1_ not NUCLE, Lang-8, W&I+LOCNESS v2.1
  url: https://www.cl.cam.ac.uk/research/nl/bea2019st/data/fce_v2.1.bea19.tar.gz
-
  problem: English GEC
  docs: https://www.cl.cam.ac.uk/research/nl/bea2019st/
  description: _W&I+LOCNESS v2.1_ not NUCLE, Lang-8, FCE v2.1
  url: https://www.cl.cam.ac.uk/research/nl/bea2019st/data/wi+locness_v2.1.bea19.tar.gz
-
  problem: Russian grammer error correction
  docs: https://github.com/arozovskaya/RULEC-GEC
  data access: https://github.com/arozovskaya/RULEC-GEC/blob/master/RULEC%20User%20Agreement.pdf
-
  problem: CONNL - NUS Corpus of Learner English (NUCLE) grammar and fluency correction
  docs: https://www.comp.nus.edu.sg/~nlp/conll14st.html#nucle32
  url: https://www.comp.nus.edu.sg/~nlp/conll14st/conll14st-test-data.tar.gz
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html
  description: Training data (42MB)
  url: http://textmining.lt/aesw/aesw2016(v1.2)_train.xml.bz2
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html 
  description: Development data (5MB)
  url: http://textmining.lt/aesw/aesw2016(v1.2)_dev.xml.bz2
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html
  description: Test data (without edits - used for the AESW 2016 Shared Task (5MB)
  url: http://textmining.lt/aesw/test.evaluation.xml.bz2
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html
  description: Test data (with edits) (6MB)
  url: http://textmining.lt/aesw/aesw2016(v1.2)_test.xml.zip).
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html 
  description: Training tokens (49MB)
  url: http://textmining.lt/aesw/aesw2016(v1.2)_train.tok.bz2
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html 
  description: Training tPOS (20MB)
  url: http://textmining.lt/aesw/aesw2016(v1.2)_train.pos.bz2
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html 
  description: Training tCFG (65MB)
  url: http://textmining.lt/aesw/aesw2016(v1.2)_train.cfg.bz2
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html 
  description: Training tDEP (186MB)
  url: http://textmining.lt/aesw/aesw2016(v1.2)_train.dep.bz2)
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html
  description: Development tokens (6MB)
  url: http://textmining.lt/aesw/aesw2016(v1.2)_dev.tok.bz2
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html 
  description: POS (3MB)
  url: http://textmining.lt/aesw/aesw2016(v1.2)_dev.pos.bz2
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html 
  description: CFG (8MB)
  url: http://textmining.lt/aesw/aesw2016(v1.2)_dev.cfg.bz2
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html 
  description: DEP (23MB)
  url: http://textmining.lt/aesw/aesw2016(v1.2)_dev.dep.bz2)
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html
  description: Test (without edits) tokens (5MB)
  url: http://textmining.lt/aesw/test.tok.bz2
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html 
  description: Test (without edits) POS (2MB)
  url: http://textmining.lt/aesw/test.pos.bz2
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html 
  description: Test (without edits) CFG (6MB)
  url: http://textmining.lt/aesw/test.cfg.bz2
- 
  problem: AESW (Automated Evaluation of Scientific Writing Data Set) grammar and fluency correction
  docs: http://textmining.lt/aesw/aesw2016down.html 
  description: Test (without edits) DEP (17MB)
  url: http://textmining.lt/aesw/test.dep.bz2)

  
