# Internship Syllabus

## Week 1

- Create account at gitlab.com (preferably with a bare e-mail and no bigtech Single-Signon)
- Find and fork an open source project (preferably tangibleai/qary, tangibleai/team, or tangibleai/tanbot)
- Edit and `commit` a README.md file or any other markdown file in the gitlab.com GUI (https://tan.sfo2.digitaloceanspaces.com/videos/howto/onboarding-02-howto-create-and-edit-markdown-files-in-gitlab-hobs-2020-11-17.mp4)
