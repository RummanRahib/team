# Nessvector Package and pynn.NNDescent indexer

Works great for close synonym substitution.

```python
>>> from nessvec.indexers import *
>>> main()
{'resources':                   start     load_hdf5  pynndescent_index  pynn_query  index_100000_vecs
 total      6.721355e+10  0.000000e+00       0.000000e+00    0.000000       6.721355e+10
 available  5.902659e+10 -1.492378e+08      -8.111350e+08    0.000000       5.821614e+10
 percent    1.220000e+01  2.000000e-01       1.200000e+00    0.000000       1.340000e+01
 used       5.676892e+09  1.280041e+08       8.410563e+08    0.000000       6.517588e+09
 free       5.587143e+10 -1.492378e+08      -8.135803e+08    0.000000       5.505855e+10
 active     1.360278e+09  1.098179e+08       2.121728e+06    0.000000       1.362399e+09
 inactive   7.864906e+09  1.813709e+07       8.428544e+08    0.000000       8.707760e+09
 buffers    3.237110e+08  8.192000e+03       4.136960e+05    0.000000       3.241247e+08
 cached     5.341512e+09  2.122547e+07      -2.788966e+07    0.000000       5.313290e+09
 shared     1.784361e+09  2.123366e+07      -2.999091e+07    0.000000       1.754038e+09
 slab       3.691397e+08  0.000000e+00       1.802240e+05    0.000000       3.693199e+08
 wall_time  1.645063e+09  7.814169e-02       4.404640e+01    0.000587       1.645063e+09,
 'index': <nessvec.indexers.Index at 0x7fe61e94f730>,
 'vecs': array([[ 0.1073,  0.0089,  0.0006, ...,  0.005 ,  0.1173, -0.04  ],
        [ 0.0897,  0.016 , -0.0571, ...,  0.1559, -0.0254, -0.0259],
        [ 0.0004,  0.0032, -0.0204, ...,  0.207 ,  0.0689, -0.0467],
        ...,
        [ 0.1364, -0.0823,  0.0268, ...,  0.0146, -0.1281,  0.1004],
        [ 0.3732,  0.0413,  0.179 , ..., -0.0461, -0.0787, -0.0635],
        [-0.0405, -0.0471,  0.1363, ...,  0.2969, -0.223 , -0.0133]],
       dtype=float32),
 'vocab': ,                0
 the              1
 .                2
 and              3
 of               4
              ...
 Bagration    99995
 NaOH         99996
 Merbabu      99997
 Husnock      99998
 Mediran      99999
 Length: 100000, dtype: int64,
 'num_vecs': 100000}
>>> index
>>> results = _
>>> results["index"]
<nessvec.indexers.Index at 0x7fe61e94f730>
>>> globals()
{'__name__': '__main__',
 '__doc__': 'Automatically created module for IPython interactive environment',
 '__package__': None,
 '__loader__': None,
 '__spec__': None,
 '__builtin__': <module 'builtins' (built-in)>,
 '__builtins__': <module 'builtins' (built-in)>,
 '_ih': ['',
  'from nessvec.indexers import *',
  'main()',
  'index',
  'results = _',
  'results["index"]',
  'globals()'],
 '_oh': {2: {'resources':                   start     load_hdf5  pynndescent_index  pynn_query  index_100000_vecs
   total      6.721355e+10  0.000000e+00       0.000000e+00    0.000000       6.721355e+10
   available  5.902659e+10 -1.492378e+08      -8.111350e+08    0.000000       5.821614e+10
   percent    1.220000e+01  2.000000e-01       1.200000e+00    0.000000       1.340000e+01
   used       5.676892e+09  1.280041e+08       8.410563e+08    0.000000       6.517588e+09
   free       5.587143e+10 -1.492378e+08      -8.135803e+08    0.000000       5.505855e+10
   active     1.360278e+09  1.098179e+08       2.121728e+06    0.000000       1.362399e+09
   inactive   7.864906e+09  1.813709e+07       8.428544e+08    0.000000       8.707760e+09
   buffers    3.237110e+08  8.192000e+03       4.136960e+05    0.000000       3.241247e+08
   cached     5.341512e+09  2.122547e+07      -2.788966e+07    0.000000       5.313290e+09
   shared     1.784361e+09  2.123366e+07      -2.999091e+07    0.000000       1.754038e+09
   slab       3.691397e+08  0.000000e+00       1.802240e+05    0.000000       3.693199e+08
   wall_time  1.645063e+09  7.814169e-02       4.404640e+01    0.000587       1.645063e+09,
   'index': <nessvec.indexers.Index at 0x7fe61e94f730>,
   'vecs': array([[ 0.1073,  0.0089,  0.0006, ...,  0.005 ,  0.1173, -0.04  ],
          [ 0.0897,  0.016 , -0.0571, ...,  0.1559, -0.0254, -0.0259],
          [ 0.0004,  0.0032, -0.0204, ...,  0.207 ,  0.0689, -0.0467],
          ...,
          [ 0.1364, -0.0823,  0.0268, ...,  0.0146, -0.1281,  0.1004],
          [ 0.3732,  0.0413,  0.179 , ..., -0.0461, -0.0787, -0.0635],
          [-0.0405, -0.0471,  0.1363, ...,  0.2969, -0.223 , -0.0133]],
         dtype=float32),
   'vocab': ,                0
   the              1
   .                2
   and              3
   of               4
                ...
   Bagration    99995
   NaOH         99996
   Merbabu      99997
   Husnock      99998
   Mediran      99999
   Length: 100000, dtype: int64,
   'num_vecs': 100000},
  5: <nessvec.indexers.Index at 0x7fe61e94f730>},
 '_dh': [PosixPath('/home/hobs/code/tangibleai/nessvec')],
 'In': ['',
  'from nessvec.indexers import *',
  'main()',
  'index',
  'results = _',
  'results["index"]',
  'globals()'],
 'Out': {2: {'resources':                   start     load_hdf5  pynndescent_index  pynn_query  index_100000_vecs
   total      6.721355e+10  0.000000e+00       0.000000e+00    0.000000       6.721355e+10
   available  5.902659e+10 -1.492378e+08      -8.111350e+08    0.000000       5.821614e+10
   percent    1.220000e+01  2.000000e-01       1.200000e+00    0.000000       1.340000e+01
   used       5.676892e+09  1.280041e+08       8.410563e+08    0.000000       6.517588e+09
   free       5.587143e+10 -1.492378e+08      -8.135803e+08    0.000000       5.505855e+10
   active     1.360278e+09  1.098179e+08       2.121728e+06    0.000000       1.362399e+09
   inactive   7.864906e+09  1.813709e+07       8.428544e+08    0.000000       8.707760e+09
   buffers    3.237110e+08  8.192000e+03       4.136960e+05    0.000000       3.241247e+08
   cached     5.341512e+09  2.122547e+07      -2.788966e+07    0.000000       5.313290e+09
   shared     1.784361e+09  2.123366e+07      -2.999091e+07    0.000000       1.754038e+09
   slab       3.691397e+08  0.000000e+00       1.802240e+05    0.000000       3.693199e+08
   wall_time  1.645063e+09  7.814169e-02       4.404640e+01    0.000587       1.645063e+09,
   'index': <nessvec.indexers.Index at 0x7fe61e94f730>,
   'vecs': array([[ 0.1073,  0.0089,  0.0006, ...,  0.005 ,  0.1173, -0.04  ],
          [ 0.0897,  0.016 , -0.0571, ...,  0.1559, -0.0254, -0.0259],
          [ 0.0004,  0.0032, -0.0204, ...,  0.207 ,  0.0689, -0.0467],
          ...,
          [ 0.1364, -0.0823,  0.0268, ...,  0.0146, -0.1281,  0.1004],
          [ 0.3732,  0.0413,  0.179 , ..., -0.0461, -0.0787, -0.0635],
          [-0.0405, -0.0471,  0.1363, ...,  0.2969, -0.223 , -0.0133]],
         dtype=float32),
   'vocab': ,                0
   the              1
   .                2
   and              3
   of               4
                ...
   Bagration    99995
   NaOH         99996
   Merbabu      99997
   Husnock      99998
   Mediran      99999
   Length: 100000, dtype: int64,
   'num_vecs': 100000},
  5: <nessvec.indexers.Index at 0x7fe61e94f730>},
 'get_ipython': <bound method InteractiveShell.get_ipython of <IPython.terminal.interactiveshell.TerminalInteractiveShell object at 0x7fe64c84eb20>>,
 'exit': <IPython.core.autocall.ExitAutocall at 0x7fe64c84e400>,
 'quit': <IPython.core.autocall.ExitAutocall at 0x7fe64c84e400>,
 '_': <nessvec.indexers.Index at 0x7fe61e94f730>,
 '__': {'resources':                   start     load_hdf5  pynndescent_index  pynn_query  index_100000_vecs
  total      6.721355e+10  0.000000e+00       0.000000e+00    0.000000       6.721355e+10
  available  5.902659e+10 -1.492378e+08      -8.111350e+08    0.000000       5.821614e+10
  percent    1.220000e+01  2.000000e-01       1.200000e+00    0.000000       1.340000e+01
  used       5.676892e+09  1.280041e+08       8.410563e+08    0.000000       6.517588e+09
  free       5.587143e+10 -1.492378e+08      -8.135803e+08    0.000000       5.505855e+10
  active     1.360278e+09  1.098179e+08       2.121728e+06    0.000000       1.362399e+09
  inactive   7.864906e+09  1.813709e+07       8.428544e+08    0.000000       8.707760e+09
  buffers    3.237110e+08  8.192000e+03       4.136960e+05    0.000000       3.241247e+08
  cached     5.341512e+09  2.122547e+07      -2.788966e+07    0.000000       5.313290e+09
  shared     1.784361e+09  2.123366e+07      -2.999091e+07    0.000000       1.754038e+09
  slab       3.691397e+08  0.000000e+00       1.802240e+05    0.000000       3.693199e+08
  wall_time  1.645063e+09  7.814169e-02       4.404640e+01    0.000587       1.645063e+09,
  'index': <nessvec.indexers.Index at 0x7fe61e94f730>,
  'vecs': array([[ 0.1073,  0.0089,  0.0006, ...,  0.005 ,  0.1173, -0.04  ],
         [ 0.0897,  0.016 , -0.0571, ...,  0.1559, -0.0254, -0.0259],
         [ 0.0004,  0.0032, -0.0204, ...,  0.207 ,  0.0689, -0.0467],
         ...,
         [ 0.1364, -0.0823,  0.0268, ...,  0.0146, -0.1281,  0.1004],
         [ 0.3732,  0.0413,  0.179 , ..., -0.0461, -0.0787, -0.0635],
         [-0.0405, -0.0471,  0.1363, ...,  0.2969, -0.223 , -0.0133]],
        dtype=float32),
  'vocab': ,                0
  the              1
  .                2
  and              3
  of               4
               ...
  Bagration    99995
  NaOH         99996
  Merbabu      99997
  Husnock      99998
  Mediran      99999
  Length: 100000, dtype: int64,
  'num_vecs': 100000},
 '___': '',
 '_i': 'results["index"]',
 '_ii': 'results = _',
 '_iii': 'index',
 '_i1': 'from nessvec.indexers import *',
 'argparse': <module 'argparse' from '/home/hobs/anaconda3/envs/nessvec/lib/python3.9/argparse.py'>,
 'logging': <module 'logging' from '/home/hobs/anaconda3/envs/nessvec/lib/python3.9/logging/__init__.py'>,
 'Path': pathlib.Path,
 'time': <module 'time' (built-in)>,
 'np': <module 'numpy' from '/home/hobs/anaconda3/envs/nessvec/lib/python3.9/site-packages/numpy/__init__.py'>,
 'pd': <module 'pandas' from '/home/hobs/anaconda3/envs/nessvec/lib/python3.9/site-packages/pandas/__init__.py'>,
 'pynn': <module 'pynndescent' from '/home/hobs/anaconda3/envs/nessvec/lib/python3.9/site-packages/pynndescent/__init__.py'>,
 'psutil': <module 'psutil' from '/home/hobs/anaconda3/envs/nessvec/lib/python3.9/site-packages/psutil/__init__.py'>,
 'DATA_DIR': PosixPath('/home/hobs/nessvec-data'),
 'LOGGING_LEVEL': 40,
 'load_hdf5': <function nessvec.files.load_hdf5(filepath='/home/hobs/nessvec-data/wiki-news-300d-1M.vec.hdf5', encoding='utf8', num_vecs=None)>,
 'download_if_necessary': <function nessvec.util.download_if_necessary(url_or_name, dest_path=None)>,
 'log': <Logger nessvec.indexers (ERROR)>,
 'Index': nessvec.indexers.Index,
 'load_analogies': <function nessvec.indexers.load_analogies(filepath='google', num_analogies=None, vocab=None)>,
 'time_and_memory': <function nessvec.indexers.time_and_memory(resources_t0=0)>,
 'index_vectors': <function nessvec.indexers.index_vectors(vecs, resources={})>,
 'init_argparse': <function nessvec.indexers.init_argparse() -> argparse.ArgumentParser>,
 'main': <function nessvec.indexers.main(args=None, num_vecs=100000, verbosity=0)>,
 '_i2': 'main()',
 '_2': {'resources':                   start     load_hdf5  pynndescent_index  pynn_query  index_100000_vecs
  total      6.721355e+10  0.000000e+00       0.000000e+00    0.000000       6.721355e+10
  available  5.902659e+10 -1.492378e+08      -8.111350e+08    0.000000       5.821614e+10
  percent    1.220000e+01  2.000000e-01       1.200000e+00    0.000000       1.340000e+01
  used       5.676892e+09  1.280041e+08       8.410563e+08    0.000000       6.517588e+09
  free       5.587143e+10 -1.492378e+08      -8.135803e+08    0.000000       5.505855e+10
  active     1.360278e+09  1.098179e+08       2.121728e+06    0.000000       1.362399e+09
  inactive   7.864906e+09  1.813709e+07       8.428544e+08    0.000000       8.707760e+09
  buffers    3.237110e+08  8.192000e+03       4.136960e+05    0.000000       3.241247e+08
  cached     5.341512e+09  2.122547e+07      -2.788966e+07    0.000000       5.313290e+09
  shared     1.784361e+09  2.123366e+07      -2.999091e+07    0.000000       1.754038e+09
  slab       3.691397e+08  0.000000e+00       1.802240e+05    0.000000       3.693199e+08
  wall_time  1.645063e+09  7.814169e-02       4.404640e+01    0.000587       1.645063e+09,
  'index': <nessvec.indexers.Index at 0x7fe61e94f730>,
  'vecs': array([[ 0.1073,  0.0089,  0.0006, ...,  0.005 ,  0.1173, -0.04  ],
         [ 0.0897,  0.016 , -0.0571, ...,  0.1559, -0.0254, -0.0259],
         [ 0.0004,  0.0032, -0.0204, ...,  0.207 ,  0.0689, -0.0467],
         ...,
         [ 0.1364, -0.0823,  0.0268, ...,  0.0146, -0.1281,  0.1004],
         [ 0.3732,  0.0413,  0.179 , ..., -0.0461, -0.0787, -0.0635],
         [-0.0405, -0.0471,  0.1363, ...,  0.2969, -0.223 , -0.0133]],
        dtype=float32),
  'vocab': ,                0
  the              1
  .                2
  and              3
  of               4
               ...
  Bagration    99995
  NaOH         99996
  Merbabu      99997
  Husnock      99998
  Mediran      99999
  Length: 100000, dtype: int64,
  'num_vecs': 100000},
 '_i3': 'index',
 '_i4': 'results = _',
 'results': {'resources':                   start     load_hdf5  pynndescent_index  pynn_query  index_100000_vecs
  total      6.721355e+10  0.000000e+00       0.000000e+00    0.000000       6.721355e+10
  available  5.902659e+10 -1.492378e+08      -8.111350e+08    0.000000       5.821614e+10
  percent    1.220000e+01  2.000000e-01       1.200000e+00    0.000000       1.340000e+01
  used       5.676892e+09  1.280041e+08       8.410563e+08    0.000000       6.517588e+09
  free       5.587143e+10 -1.492378e+08      -8.135803e+08    0.000000       5.505855e+10
  active     1.360278e+09  1.098179e+08       2.121728e+06    0.000000       1.362399e+09
  inactive   7.864906e+09  1.813709e+07       8.428544e+08    0.000000       8.707760e+09
  buffers    3.237110e+08  8.192000e+03       4.136960e+05    0.000000       3.241247e+08
  cached     5.341512e+09  2.122547e+07      -2.788966e+07    0.000000       5.313290e+09
  shared     1.784361e+09  2.123366e+07      -2.999091e+07    0.000000       1.754038e+09
  slab       3.691397e+08  0.000000e+00       1.802240e+05    0.000000       3.693199e+08
  wall_time  1.645063e+09  7.814169e-02       4.404640e+01    0.000587       1.645063e+09,
  'index': <nessvec.indexers.Index at 0x7fe61e94f730>,
  'vecs': array([[ 0.1073,  0.0089,  0.0006, ...,  0.005 ,  0.1173, -0.04  ],
         [ 0.0897,  0.016 , -0.0571, ...,  0.1559, -0.0254, -0.0259],
         [ 0.0004,  0.0032, -0.0204, ...,  0.207 ,  0.0689, -0.0467],
         ...,
         [ 0.1364, -0.0823,  0.0268, ...,  0.0146, -0.1281,  0.1004],
         [ 0.3732,  0.0413,  0.179 , ..., -0.0461, -0.0787, -0.0635],
         [-0.0405, -0.0471,  0.1363, ...,  0.2969, -0.223 , -0.0133]],
        dtype=float32),
  'vocab': ,                0
  the              1
  .                2
  and              3
  of               4
               ...
  Bagration    99995
  NaOH         99996
  Merbabu      99997
  Husnock      99998
  Mediran      99999
  Length: 100000, dtype: int64,
  'num_vecs': 100000},
 '_i5': 'results["index"]',
 '_5': <nessvec.indexers.Index at 0x7fe61e94f730>,
 '_i6': 'globals()'}
>>> globals().keys()
dict_keys(['__name__', '__doc__', '__package__', '__loader__', '__spec__', '__builtin__', '__builtins__', '_ih', '_oh', '_dh', 'In', 'Out', 'get_ipython', 'exit', 'quit', '_', '__', '___', '_i', '_ii', '_iii', '_i1', 'argparse', 'logging', 'Path', 'time', 'np', 'pd', 'pynn', 'psutil', 'DATA_DIR', 'LOGGING_LEVEL', 'load_hdf5', 'download_if_necessary', 'log', 'Index', 'load_analogies', 'time_and_memory', 'index_vectors', 'init_argparse', 'main', '_i2', '_2', '_i3', '_i4', 'results', '_i5', '_5', '_i6', '_6', '_i7'])
>>> who
>>> index.query?
>>> index = results["index"]
>>> index["kemal"]
>>> index.query?
>>> index.query("kemal")
>>> vocab["kemal"]
>>> vocab = results["vocab"]
>>> vocab
,                0
the              1
.                2
and              3
of               4
             ...
Bagration    99995
NaOH         99996
Merbabu      99997
Husnock      99998
Mediran      99999
Length: 100000, dtype: int64
>>> vocab["Kemal"]
47565
>>> vocab["kemal"]
>>> vocab["Kemal"]
47565
>>> index.query(vocab["Kemal"])
>>> vecs = results["vecs"]
>>> vecs[vocab["Kemal"]]
array([ 0.0102, -0.162 , -0.1435, -0.1247, -0.0418,  0.033 ,  0.1222,
        0.122 , -0.0304,  0.1185, -0.0863,  0.0747, -0.1028, -0.1861,
        0.084 ,  0.0019, -0.1152, -0.0078,  0.2136,  0.0194, -0.2103,
        0.1622, -0.0907,  0.0374,  0.107 ,  0.0242,  0.1417, -0.0697,
        0.2034, -0.0854, -0.0636, -0.0687,  0.0771, -0.2466, -0.0365,
        0.2498, -0.1452, -0.068 ,  0.0205, -0.01  , -0.0924,  0.0705,
       -0.0759, -0.0439, -0.0715,  0.0081,  0.1372, -0.0778, -0.0051,
       -0.0046,  0.015 ,  0.0755, -0.8329,  0.0029,  0.1214, -0.0757,
        0.1961, -0.1851, -0.0305, -0.0147,  0.041 ,  0.0761, -0.0317,
        0.1229,  0.0374,  0.0736, -0.1053,  0.3141, -0.0577, -0.1266,
        0.0144,  0.0985,  0.1876, -0.0186,  0.1338,  0.2346,  0.0967,
        0.0357,  0.0418,  0.0045,  0.0641,  0.0619, -0.0383, -0.2711,
       -0.0766,  0.1324, -0.0101, -0.1979, -0.0154,  0.0714, -0.0168,
        0.0566, -0.0156, -0.1532, -0.0787, -0.1243,  0.029 , -0.2285,
        0.2126,  0.1787,  0.0043,  0.0193, -0.0871,  0.0731, -0.0474,
        0.0739,  0.0912, -0.1399,  0.0672,  0.1547,  0.0834,  0.1044,
        0.0193,  0.0637,  0.2411,  0.013 , -0.1794, -0.3476, -0.091 ,
       -0.4013,  0.1543,  0.2649, -0.0326, -0.1727, -0.0463,  0.0411,
        0.1614,  0.0967,  0.4448,  0.2435,  0.0104,  0.0708,  0.0967,
        0.0758,  0.0618, -0.1706,  0.0999,  0.1981,  0.0024,  0.0301,
       -0.1135,  0.0315,  0.1577,  0.1802,  0.0493, -0.2196,  0.0068,
       -0.0927, -0.0128, -0.0058,  0.1053, -0.1435,  0.4109,  0.1728,
       -0.0763,  0.1549,  0.0855, -0.145 ,  0.1202,  0.0136, -0.1595,
        0.1161, -0.0173,  0.0579,  0.086 ,  0.2744,  0.0406, -0.1174,
       -0.0519,  0.0167, -0.0772,  0.1948,  0.0497,  0.2075, -0.1178,
        0.1889,  0.3142, -0.0774,  0.0282,  0.0748,  0.0743, -0.0828,
       -0.2632,  0.0147, -0.3595,  0.0685,  0.1796,  0.0849, -0.1709,
        0.1061, -0.1899, -0.1556,  0.0336,  0.1106,  0.1249, -0.1323,
        0.1137, -0.0411,  0.0796, -0.095 , -0.0604, -0.0291,  0.1195,
        0.1402, -0.0589, -0.0794, -0.0575, -0.1529, -0.0236,  0.0637,
       -0.0508,  0.0501, -0.1341, -0.0531, -0.1043, -0.0626,  0.0236,
       -0.0267, -0.2264, -0.2261,  0.1145, -0.0475,  0.2177, -0.0427,
       -0.0991, -0.    ,  0.2291, -0.1062, -0.18  ,  0.034 ,  0.3365,
       -0.111 ,  0.2342, -0.209 ,  0.2332, -0.3378, -0.0559,  0.2197,
       -0.1724, -0.06  , -0.2079,  0.0918, -0.0295, -0.043 , -0.0932,
        0.1538,  0.2817,  0.0743,  0.1343,  0.3511, -0.171 ,  0.0683,
        0.0218,  0.1131, -0.1211, -0.1489, -0.138 , -0.0566, -0.0924,
       -0.0583, -0.0433, -0.079 ,  0.1911, -0.0602, -0.2856, -0.3835,
        0.1344,  0.1384,  0.3312, -0.0289, -0.2036, -0.2223,  0.0152,
        0.0459, -0.1563, -0.0415, -0.0972,  0.0325,  0.0521,  0.0243,
        0.2483, -0.0498,  0.121 , -0.1636,  0.1709, -0.0829,  0.0068,
       -0.0219, -0.1325, -0.0044, -0.0121, -0.0223, -0.0789, -0.0886,
       -0.141 ,  0.2076, -0.0308, -0.1299, -0.0262,  0.0566],
      dtype=float32)
>>> index.query(vecs[vocab["Kemal"]])
(array([[47565, 19600, 39879, 83805, 51418, 79684, 50904, 46042, 82758,
         33405]], dtype=int32),
 array([[2.38418528e-07, 2.93713655e-01, 3.38687099e-01, 3.59308331e-01,
         3.69054203e-01, 3.69857739e-01, 3.83065608e-01, 3.88357316e-01,
         3.94704423e-01, 3.95325769e-01]]))
>>> index.query(vecs[vocab["Kemal"]])[0][0]
array([47565, 19600, 39879, 83805, 51418, 79684, 50904, 46042, 82758,
       33405], dtype=int32)
>>> vocab.iloc[index.query(vecs[vocab["Kemal"]])[0][0]]
Kemal      47565
Mustafa    19600
Ahmet      39879
Orhan      83805
Atatürk    51418
Enver      79684
Ataturk    50904
Arif       46042
Hakan      82758
Mehmet     33405
dtype: int64
>>> "We all are one."
'We all are one.'
>>> import spcay
>>> import spacy
>>> s = "We all are one .".split()
>>> toks = s
>>> def get_nearest(index, vocab):
...     pass
...
>>> index.data
>>> dir(index)
['__class__',
 '__delattr__',
 '__dict__',
 '__dir__',
 '__doc__',
 '__eq__',
 '__format__',
 '__ge__',
 '__getattribute__',
 '__getstate__',
 '__gt__',
 '__hash__',
 '__init__',
 '__init_subclass__',
 '__le__',
 '__lt__',
 '__module__',
 '__ne__',
 '__new__',
 '__reduce__',
 '__reduce_ex__',
 '__repr__',
 '__setattr__',
 '__setstate__',
 '__sizeof__',
 '__str__',
 '__subclasshook__',
 '__weakref__',
 '_angular_trees',
 '_dist_args',
 '_distance_correction',
 '_distance_func',
 '_init_search_function',
 '_init_search_graph',
 '_init_sparse_search_function',
 '_is_sparse',
 '_original_num_threads',
 '_raw_data',
 '_search_forest',
 '_search_function',
 '_search_graph',
 '_tree_search',
 '_vertex_order',
 '_visited',
 'compress_index',
 'compressed',
 'delta',
 'dim',
 'diversify_prob',
 'leaf_size',
 'low_memory',
 'max_candidates',
 'metric',
 'metric_kwds',
 'n_iters',
 'n_jobs',
 'n_neighbors',
 'n_search_trees',
 'n_trees',
 'neighbor_graph',
 'prepare',
 'prune_degree_multiplier',
 'query',
 'random_state',
 'rng_state',
 'search_rng_state',
 'tree_init',
 'update',
 'verbose']
>>> index._raw_data
array([[ 0.1323, -0.1685, -0.0373, ...,  0.1717, -0.1049, -0.0398],
       [-0.0027, -0.0186, -0.2529, ...,  0.0964, -0.0756,  0.2636],
       [ 0.2213, -0.125 ,  0.0603, ...,  0.2068, -0.1887, -0.1345],
       ...,
       [-0.0133, -0.1195,  0.0324, ...,  0.2961, -0.0595, -0.1557],
       [ 0.3106,  0.0361,  0.1978, ...,  0.2314,  0.1043, -0.0428],
       [ 0.0907, -0.2081,  0.0487, ...,  0.1677,  0.0087, -0.1209]],
      dtype=float32)
>>> len(index._raw_data)
100000
>>> index._raw_data.shape
(100000, 300)
>>> def get_nearest(word, index=index, vocab=vocab):
...     vocab.iloc[index.query(vecs[vocab[word]])[0][0]]
...
>>> get_nearest("Kemal")
>>> def get_nearest(word, index=index, vocab=vocab):
...     return vocab.iloc[index.query(vecs[vocab[word]])[0][0]]
...
>>> get_nearest("Kemal")
Kemal      47565
Mustafa    19600
Ahmet      39879
Orhan      83805
Atatürk    51418
Enver      79684
Ataturk    50904
Arif       46042
Hakan      82758
Mehmet     33405
dtype: int64
>>> type(get_nearest("Kemal"))
pandas.core.series.Series
>>> get_nearest("Kemal").index
Index(['Kemal', 'Mustafa', 'Ahmet', 'Orhan', 'Atatürk', 'Enver', 'Ataturk',
       'Arif', 'Hakan', 'Mehmet'],
      dtype='object')
>>> get_nearest("Kemal").index.values
array(['Kemal', 'Mustafa', 'Ahmet', 'Orhan', 'Atatürk', 'Enver',
       'Ataturk', 'Arif', 'Hakan', 'Mehmet'], dtype=object)
>>> def get_nearest(word, index=index, vocab=vocab):
...     return vocab.iloc[index.query(vecs[vocab[word]])[0][0]].index.values
...
>>> for tok in toks:
...     []
...
>>> vocab.iloc[index.query(vecs[vocab["Kemal"]])[0][0]]
Kemal      47565
Mustafa    19600
Ahmet      39879
Orhan      83805
Atatürk    51418
Enver      79684
Ataturk    50904
Arif       46042
Hakan      82758
Mehmet     33405
dtype: int64
>>> index.query(vecs[vocab["Kemal"]])[0][0]
array([47565, 19600, 39879, 83805, 51418, 79684, 50904, 46042, 82758,
       33405], dtype=int32)
>>> index.query(vecs[vocab["Kemal"]])[0]
array([[47565, 19600, 39879, 83805, 51418, 79684, 50904, 46042, 82758,
        33405]], dtype=int32)
>>> index.query(vecs[vocab["Kemal"]])
(array([[47565, 19600, 39879, 83805, 51418, 79684, 50904, 46042, 82758,
         33405]], dtype=int32),
 array([[2.38418528e-07, 2.93713655e-01, 3.38687099e-01, 3.59308331e-01,
         3.69054203e-01, 3.69857739e-01, 3.83065608e-01, 3.88357316e-01,
         3.94704423e-01, 3.95325769e-01]]))
>>> index.query(vecs[vocab["Kemal"]])[1][0]
array([2.38418528e-07, 2.93713655e-01, 3.38687099e-01, 3.59308331e-01,
       3.69054203e-01, 3.69857739e-01, 3.83065608e-01, 3.88357316e-01,
       3.94704423e-01, 3.95325769e-01])
>>> qresults = index.query(vecs[vocab["Kemal"]])
>>> words = vocab.iloc[qresults[0][0]]]
>>> words = vocab.iloc[qresults[0][0]]
>>> words
Kemal      47565
Mustafa    19600
Ahmet      39879
Orhan      83805
Atatürk    51418
Enver      79684
Ataturk    50904
Arif       46042
Hakan      82758
Mehmet     33405
dtype: int64
>>> similarities = qresults[1][0]
>>> similarities
array([2.38418528e-07, 2.93713655e-01, 3.38687099e-01, 3.59308331e-01,
       3.69054203e-01, 3.69857739e-01, 3.83065608e-01, 3.88357316e-01,
       3.94704423e-01, 3.95325769e-01])
>>> def get_nearest(word, index=index, vocab=vocab):
...     qresults = index.query(vecs[vocab["Kemal"]])
...     words = vocab.iloc[qresults[0][0]]
...     similarities = qresults[1][0]
...     df = DataFrame(words)
...     df["similarities"] = qresults[1][0]
...     return df
...
>>> def get_nearest(word, index=index, vocab=vocab):
...     qresults = index.query(vecs[vocab[word]])
...     words = vocab.iloc[qresults[0][0]]
...     similarities = qresults[1][0]
...     df = DataFrame(words)
...     df["similarities"] = qresults[1][0]
...     return pd.Series(similarities, index=words)
...
>>> qresults = index.query(vecs[vocab["Kemal"]])
>>> qresults
(array([[47565, 19600, 39879, 83805, 51418, 79684, 50904, 46042, 82758,
         33405]], dtype=int32),
 array([[2.38418528e-07, 2.93713655e-01, 3.38687099e-01, 3.59308331e-01,
         3.69054203e-01, 3.69857739e-01, 3.83065608e-01, 3.88357316e-01,
         3.94704423e-01, 3.95325769e-01]]))
>>> get_nearest("Kemal").index.values
>>> get_nearest("Kemal")
>>> def get_nearest(word, index=index, vocab=vocab):
...     qresults = index.query(vecs[vocab[word]])
...     words = vocab.iloc[qresults[0][0]]
...     similarities = qresults[1][0]
...     df = pd.DataFrame(words)
...     df["similarities"] = qresults[1][0]
...     return pd.Series(similarities, index=words)
...
>>> get_nearest("Kemal")
47565    2.384185e-07
19600    2.937137e-01
39879    3.386871e-01
83805    3.593083e-01
51418    3.690542e-01
79684    3.698577e-01
50904    3.830656e-01
46042    3.883573e-01
82758    3.947044e-01
33405    3.953258e-01
dtype: float64
>>> def get_nearest(word, index=index, vocab=vocab):
...     qresults = index.query(vecs[vocab[word]])
...     words = vocab.iloc[qresults[0][0]]
...     similarities = qresults[1][0]
...     df = pd.DataFrame(words)
...     df["similarities"] = qresults[1][0]
...     return pd.Series(similarities, index=words.index)
...
>>> get_nearest("Kemal")
Kemal      2.384185e-07
Mustafa    2.937137e-01
Ahmet      3.386871e-01
Orhan      3.593083e-01
Atatürk    3.690542e-01
Enver      3.698577e-01
Ataturk    3.830656e-01
Arif       3.883573e-01
Hakan      3.947044e-01
Mehmet     3.953258e-01
dtype: float64
>>> get_nearest("Kemal").round(2)
Kemal      0.00
Mustafa    0.29
Ahmet      0.34
Orhan      0.36
Atatürk    0.37
Enver      0.37
Ataturk    0.38
Arif       0.39
Hakan      0.39
Mehmet     0.40
dtype: float64
>>> [
...     get_nearest(tok).index.values[1] if get_nearest(tok).iloc[1] < 0.1 else tok
...     for tok in toks
... ]
...
['We', 'all', 'are', 'one', '.']
>>> [
...     get_nearest(tok).index.values[1] if get_nearest(tok).iloc[1] < 0.5 else tok
...     for tok in toks
... ]
...
['You', 'these', 'were', 'another', 'though']
>>> [
...     get_nearest(tok).index.values[1] if get_nearest(tok).iloc[1] < 0.3 else tok
...     for tok in toks
... ]
...
['You', 'these', 'were', 'another', '.']
>>> [
...     get_nearest(tok).index.values[1] if get_nearest(tok).iloc[1] < 0.2 else tok
...     for tok in toks
... ]
...
['We', 'all', 'were', 'one', '.']
>>> def reword_sentence(sent):
...     pass
...
>>> hist
>>> def reword_sentence(sent, max_dist=0.2):
...     return " ".join(
...         [
...             get_nearest(tok).index.values[1]
...             if get_nearest(tok).iloc[1] < max_dist
...             else tok
...             for tok in toks
...         ]
...     )
...
>>> reword_sentence("I was here .")
'We all were one .'
>>> def reword_sentence(sent, max_dist=0.2):
...     return " ".join(
...         [
...             get_nearest(tok).index.values[1]
...             if get_nearest(tok).iloc[1] < max_dist
...             else tok
...             for tok in sent.split()
...         ]
...     )
...
>>> reword_sentence("Everything will be better .")
'Everything must be better .'
>>> reword_sentence("And if we don't know it , we will find out the hard way .")
>>> reword_sentence("And if we do n't know it , we will find out the hard way .")
>>> reword_sentence("And if we dont know it , we will find out the hard way .")
'But if we dont know it , we must find out the hard way .'
>>> hist
>>> def extract_entities(
...     sent,
...     examples="Australia USA PNG France China Indonesia India Congo Etheopia".split(),
...     max_dist=0.2,
... ):
...     for tok in sent.split():
...         get_nearest(tok)
...     [
...         get_nearest(tok).index.values[1] if get_nearest(tok).iloc[1] < max_dist else tok
...         for tok in sent.split()
...     ]
...
>>> examples="Australia USA PNG France China Indonesia India Congo Etheopia".split()
>>> get_nearest(examples[0])
Australia     -1.192093e-07
Melbourne      2.522458e-01
Australian     2.674877e-01
Sydney         2.781407e-01
Queensland     2.834466e-01
Australasia    2.941692e-01
Tasmania       3.028041e-01
Canada         3.033484e-01
Zealand        3.040299e-01
Brisbane       3.182869e-01
dtype: float64
>>> get_nearest(examples[1])
USA        0.000000
U.S.A.     0.168234
U.S.A      0.197813
US         0.292445
UK         0.293490
U.S        0.322788
Germany    0.341817
Canada     0.344278
Europe     0.356791
America    0.366055
dtype: float64
>>> possible_countries = []
>>> possible_countries += list(get_nearest(examples[1]))
>>> possible_countries += list(get_nearest(examples[0]))
>>> possible_countries += list(get_nearest(examples[0]))
>>> for i in range(len(examples)):
...     possible_countries += list(get_nearest(examples[i]))
...
>>> hist
>>> possible_countries
[0.0,
 0.16823391224368844,
 0.1978125635642245,
 0.2924446421614092,
 0.29348957487542915,
 0.32278773557452434,
 0.34181653781441124,
 0.3442781118957252,
 0.356790861580783,
 0.3660554482451256,
 -1.19209304871859e-07,
 0.2522457944862052,
 0.267487741693775,
 0.2781407458049503,
 0.28344662154055233,
 0.2941692423957504,
 0.30280411770715954,
 0.3033483678743988,
 0.3040299377940928,
 0.31828693484524584,
 -1.19209304871859e-07,
 0.2522457944862052,
 0.267487741693775,
 0.2781407458049503,
 0.28344662154055233,
 0.2941692423957504,
 0.30280411770715954,
 0.3033483678743988,
 0.3040299377940928,
 0.31828693484524584,
 -1.19209304871859e-07,
 0.2522457944862052,
 0.267487741693775,
 0.2781407458049503,
 0.28344662154055233,
 0.2941692423957504,
 0.30280411770715954,
 0.3033483678743988,
 0.3040299377940928,
 0.31828693484524584,
 0.0,
 0.16823391224368844,
 0.1978125635642245,
 0.2924446421614092,
 0.29348957487542915,
 0.32278773557452434,
 0.34181653781441124,
 0.3442781118957252,
 0.356790861580783,
 0.3660554482451256,
 2.3841852780925876e-07,
 0.3439828075600718,
 0.3667670325244935,
 0.3824537075342842,
 0.4024741451366639,
 0.4069522759609605,
 0.41441170589148923,
 0.4282394465337255,
 0.4504440424690017,
 0.474648564287805,
 0.0,
 0.2542581288282624,
 0.2580003543742959,
 0.28151762172804706,
 0.2875559862045832,
 0.2918726086949016,
 0.2927585880283098,
 0.30600473485625546,
 0.30990021425316927,
 0.3143098830434322,
 1.1920927101005674e-07,
 0.20706960916537276,
 0.25289751360013,
 0.2751179250794723,
 0.2760255401658057,
 0.28339765443235365,
 0.286804098122174,
 0.28968293381774124,
 0.3118439401793547,
 0.3202247605674705,
 -1.788139627478813e-07,
 0.18858287641770133,
 0.20450263474859065,
 0.268864740541526,
 0.2786235752665829,
 0.29635989915286787,
 0.32450039204812686,
 0.3279302984990281,
 0.34060352379249226,
 0.3479288283381702,
 0.0,
 0.25289757533289436,
 0.2543595490104277,
 0.27262542078569096,
 0.29196215606329046,
 0.3084550034360227,
 0.3130559097486124,
 0.32165221293338153,
 0.32286503681744394,
 0.33154591588605253,
 1.1920927101005674e-07,
 0.22537361672691236,
 0.2672611836848793,
 0.28306986761308395,
 0.31059542621285974,
 0.3353975719236003,
 0.3482044489792294,
 0.3589387291688769,
 0.35965494189000113,
 0.3604120799911579]
>>> for i in range(len(examples)):
...     possible_countries += list(get_nearest(examples[i]).index.values)
...
>>> possible_countries
[0.0,
 0.16823391224368844,
 0.1978125635642245,
 0.2924446421614092,
 0.29348957487542915,
 0.32278773557452434,
 0.34181653781441124,
 0.3442781118957252,
 0.356790861580783,
 0.3660554482451256,
 -1.19209304871859e-07,
 0.2522457944862052,
 0.267487741693775,
 0.2781407458049503,
 0.28344662154055233,
 0.2941692423957504,
 0.30280411770715954,
 0.3033483678743988,
 0.3040299377940928,
 0.31828693484524584,
 -1.19209304871859e-07,
 0.2522457944862052,
 0.267487741693775,
 0.2781407458049503,
 0.28344662154055233,
 0.2941692423957504,
 0.30280411770715954,
 0.3033483678743988,
 0.3040299377940928,
 0.31828693484524584,
 -1.19209304871859e-07,
 0.2522457944862052,
 0.267487741693775,
 0.2781407458049503,
 0.28344662154055233,
 0.2941692423957504,
 0.30280411770715954,
 0.3033483678743988,
 0.3040299377940928,
 0.31828693484524584,
 0.0,
 0.16823391224368844,
 0.1978125635642245,
 0.2924446421614092,
 0.29348957487542915,
 0.32278773557452434,
 0.34181653781441124,
 0.3442781118957252,
 0.356790861580783,
 0.3660554482451256,
 2.3841852780925876e-07,
 0.3439828075600718,
 0.3667670325244935,
 0.3824537075342842,
 0.4024741451366639,
 0.4069522759609605,
 0.41441170589148923,
 0.4282394465337255,
 0.4504440424690017,
 0.474648564287805,
 0.0,
 0.2542581288282624,
 0.2580003543742959,
 0.28151762172804706,
 0.2875559862045832,
 0.2918726086949016,
 0.2927585880283098,
 0.30600473485625546,
 0.30990021425316927,
 0.3143098830434322,
 1.1920927101005674e-07,
 0.20706960916537276,
 0.25289751360013,
 0.2751179250794723,
 0.2760255401658057,
 0.28339765443235365,
 0.286804098122174,
 0.28968293381774124,
 0.3118439401793547,
 0.3202247605674705,
 -1.788139627478813e-07,
 0.18858287641770133,
 0.20450263474859065,
 0.268864740541526,
 0.2786235752665829,
 0.29635989915286787,
 0.32450039204812686,
 0.3279302984990281,
 0.34060352379249226,
 0.3479288283381702,
 0.0,
 0.25289757533289436,
 0.2543595490104277,
 0.27262542078569096,
 0.29196215606329046,
 0.3084550034360227,
 0.3130559097486124,
 0.32165221293338153,
 0.32286503681744394,
 0.33154591588605253,
 1.1920927101005674e-07,
 0.22537361672691236,
 0.2672611836848793,
 0.28306986761308395,
 0.31059542621285974,
 0.3353975719236003,
 0.3482044489792294,
 0.3589387291688769,
 0.35965494189000113,
 0.3604120799911579,
 'Australia',
 'Melbourne',
 'Australian',
 'Sydney',
 'Queensland',
 'Australasia',
 'Tasmania',
 'Canada',
 'Zealand',
 'Brisbane',
 'USA',
 'U.S.A.',
 'U.S.A',
 'US',
 'UK',
 'U.S',
 'Germany',
 'Canada',
 'Europe',
 'America',
 'PNG',
 'png',
 'SVG',
 'Papua',
 'PNGs',
 'Nauru',
 'JPG',
 'JPEG',
 'Melanesian',
 'SVGs',
 'France',
 'Paris',
 'Germany',
 'Italy',
 'Britain',
 'England',
 'Spain',
 'france',
 'Belgium',
 'Europe',
 'China',
 'Beijing',
 'India',
 'Shanghai',
 'Chinese',
 'Taiwan',
 'Russia',
 'Japan',
 'PRC',
 'Korea',
 'Indonesia',
 'Jakarta',
 'Indonesian',
 'Malaysia',
 'Indonesians',
 'Sumatra',
 'Aceh',
 'Sulawesi',
 'Papua',
 'Yogyakarta',
 'India',
 'China',
 'Pakistan',
 'Delhi',
 'subcontinent',
 'Mumbai',
 'india',
 'Hyderabad',
 'Bangalore',
 'Indian',
 'Congo',
 'Congolese',
 'Kinshasa',
 'DRC',
 'Zaire',
 'Katanga',
 'Brazzaville',
 'Kabila',
 'Rwanda',
 'Gabon']
>>> for i in range(len(examples)):
...     possible_countries += list(get_nearest(examples[i]).index.values)
...
>>> Ethiopia
>>> examples = "Australia USA PNG France China Indonesia India Congo Ethiopia".split()
>>> for i in range(len(examples)):
...
...     possible_countries += list(get_nearest(examples[i]).index.values)
...
>>> possible_countries
[0.0,
 0.16823391224368844,
 0.1978125635642245,
 0.2924446421614092,
 0.29348957487542915,
 0.32278773557452434,
 0.34181653781441124,
 0.3442781118957252,
 0.356790861580783,
 0.3660554482451256,
 -1.19209304871859e-07,
 0.2522457944862052,
 0.267487741693775,
 0.2781407458049503,
 0.28344662154055233,
 0.2941692423957504,
 0.30280411770715954,
 0.3033483678743988,
 0.3040299377940928,
 0.31828693484524584,
 -1.19209304871859e-07,
 0.2522457944862052,
 0.267487741693775,
 0.2781407458049503,
 0.28344662154055233,
 0.2941692423957504,
 0.30280411770715954,
 0.3033483678743988,
 0.3040299377940928,
 0.31828693484524584,
 -1.19209304871859e-07,
 0.2522457944862052,
 0.267487741693775,
 0.2781407458049503,
 0.28344662154055233,
 0.2941692423957504,
 0.30280411770715954,
 0.3033483678743988,
 0.3040299377940928,
 0.31828693484524584,
 0.0,
 0.16823391224368844,
 0.1978125635642245,
 0.2924446421614092,
 0.29348957487542915,
 0.32278773557452434,
 0.34181653781441124,
 0.3442781118957252,
 0.356790861580783,
 0.3660554482451256,
 2.3841852780925876e-07,
 0.3439828075600718,
 0.3667670325244935,
 0.3824537075342842,
 0.4024741451366639,
 0.4069522759609605,
 0.41441170589148923,
 0.4282394465337255,
 0.4504440424690017,
 0.474648564287805,
 0.0,
 0.2542581288282624,
 0.2580003543742959,
 0.28151762172804706,
 0.2875559862045832,
 0.2918726086949016,
 0.2927585880283098,
 0.30600473485625546,
 0.30990021425316927,
 0.3143098830434322,
 1.1920927101005674e-07,
 0.20706960916537276,
 0.25289751360013,
 0.2751179250794723,
 0.2760255401658057,
 0.28339765443235365,
 0.286804098122174,
 0.28968293381774124,
 0.3118439401793547,
 0.3202247605674705,
 -1.788139627478813e-07,
 0.18858287641770133,
 0.20450263474859065,
 0.268864740541526,
 0.2786235752665829,
 0.29635989915286787,
 0.32450039204812686,
 0.3279302984990281,
 0.34060352379249226,
 0.3479288283381702,
 0.0,
 0.25289757533289436,
 0.2543595490104277,
 0.27262542078569096,
 0.29196215606329046,
 0.3084550034360227,
 0.3130559097486124,
 0.32165221293338153,
 0.32286503681744394,
 0.33154591588605253,
 1.1920927101005674e-07,
 0.22537361672691236,
 0.2672611836848793,
 0.28306986761308395,
 0.31059542621285974,
 0.3353975719236003,
 0.3482044489792294,
 0.3589387291688769,
 0.35965494189000113,
 0.3604120799911579,
 'Australia',
 'Melbourne',
 'Australian',
 'Sydney',
 'Queensland',
 'Australasia',
 'Tasmania',
 'Canada',
 'Zealand',
 'Brisbane',
 'USA',
 'U.S.A.',
 'U.S.A',
 'US',
 'UK',
 'U.S',
 'Germany',
 'Canada',
 'Europe',
 'America',
 'PNG',
 'png',
 'SVG',
 'Papua',
 'PNGs',
 'Nauru',
 'JPG',
 'JPEG',
 'Melanesian',
 'SVGs',
 'France',
 'Paris',
 'Germany',
 'Italy',
 'Britain',
 'England',
 'Spain',
 'france',
 'Belgium',
 'Europe',
 'China',
 'Beijing',
 'India',
 'Shanghai',
 'Chinese',
 'Taiwan',
 'Russia',
 'Japan',
 'PRC',
 'Korea',
 'Indonesia',
 'Jakarta',
 'Indonesian',
 'Malaysia',
 'Indonesians',
 'Sumatra',
 'Aceh',
 'Sulawesi',
 'Papua',
 'Yogyakarta',
 'India',
 'China',
 'Pakistan',
 'Delhi',
 'subcontinent',
 'Mumbai',
 'india',
 'Hyderabad',
 'Bangalore',
 'Indian',
 'Congo',
 'Congolese',
 'Kinshasa',
 'DRC',
 'Zaire',
 'Katanga',
 'Brazzaville',
 'Kabila',
 'Rwanda',
 'Gabon',
 'Australia',
 'Melbourne',
 'Australian',
 'Sydney',
 'Queensland',
 'Australasia',
 'Tasmania',
 'Canada',
 'Zealand',
 'Brisbane',
 'USA',
 'U.S.A.',
 'U.S.A',
 'US',
 'UK',
 'U.S',
 'Germany',
 'Canada',
 'Europe',
 'America',
 'PNG',
 'png',
 'SVG',
 'Papua',
 'PNGs',
 'Nauru',
 'JPG',
 'JPEG',
 'Melanesian',
 'SVGs',
 'France',
 'Paris',
 'Germany',
 'Italy',
 'Britain',
 'England',
 'Spain',
 'france',
 'Belgium',
 'Europe',
 'China',
 'Beijing',
 'India',
 'Shanghai',
 'Chinese',
 'Taiwan',
 'Russia',
 'Japan',
 'PRC',
 'Korea',
 'Indonesia',
 'Jakarta',
 'Indonesian',
 'Malaysia',
 'Indonesians',
 'Sumatra',
 'Aceh',
 'Sulawesi',
 'Papua',
 'Yogyakarta',
 'India',
 'China',
 'Pakistan',
 'Delhi',
 'subcontinent',
 'Mumbai',
 'india',
 'Hyderabad',
 'Bangalore',
 'Indian',
 'Congo',
 'Congolese',
 'Kinshasa',
 'DRC',
 'Zaire',
 'Katanga',
 'Brazzaville',
 'Kabila',
 'Rwanda',
 'Gabon',
 'Australia',
 'Melbourne',
 'Australian',
 'Sydney',
 'Queensland',
 'Australasia',
 'Tasmania',
 'Canada',
 'Zealand',
 'Brisbane',
 'USA',
 'U.S.A.',
 'U.S.A',
 'US',
 'UK',
 'U.S',
 'Germany',
 'Canada',
 'Europe',
 'America',
 'PNG',
 'png',
 'SVG',
 'Papua',
 'PNGs',
 'Nauru',
 'JPG',
 'JPEG',
 'Melanesian',
 'SVGs',
 'France',
 'Paris',
 'Germany',
 'Italy',
 'Britain',
 'England',
 'Spain',
 'france',
 'Belgium',
 'Europe',
 'China',
 'Beijing',
 'India',
 'Shanghai',
 'Chinese',
 'Taiwan',
 'Russia',
 'Japan',
 'PRC',
 'Korea',
 'Indonesia',
 'Jakarta',
 'Indonesian',
 'Malaysia',
 'Indonesians',
 'Sumatra',
 'Aceh',
 'Sulawesi',
 'Papua',
 'Yogyakarta',
 'India',
 'China',
 'Pakistan',
 'Delhi',
 'subcontinent',
 'Mumbai',
 'india',
 'Hyderabad',
 'Bangalore',
 'Indian',
 'Congo',
 'Congolese',
 'Kinshasa',
 'DRC',
 'Zaire',
 'Katanga',
 'Brazzaville',
 'Kabila',
 'Rwanda',
 'Gabon',
 'Ethiopia',
 'Eritrea',
 'Ethiopian',
 'Ethiopians',
 'Tigray',
 'Somalia',
 'Eritrean',
 'Amharic',
 'Ababa',
 'Djibouti']
>>> for i in range(len(examples)):
...
...     possible_countries += list(get_nearest(examples[i]).index.values)
...
```

Experiment with `set.union()` to use instead of appending to a list.

```python
>>> set("abcd") + set("cdefg")
>>> set("abcd").union(set("cdefg"))
{'a', 'b', 'c', 'd', 'e', 'f', 'g'}
>>> np.arange(3)
array([0, 1, 2])
```

Unable to .update() index with new vectors due to bug in PyNNDescent package.

```python
>>> index
<nessvec.indexers.Index at 0x7fe61e94f730>
>>> dir(index)
['__class__',
 '__delattr__',
 '__dict__',
 '__dir__',
 '__doc__',
 '__eq__',
 '__format__',
 '__ge__',
 '__getattribute__',
 '__getstate__',
 '__gt__',
 '__hash__',
 '__init__',
 '__init_subclass__',
 '__le__',
 '__lt__',
 '__module__',
 '__ne__',
 '__new__',
 '__reduce__',
 '__reduce_ex__',
 '__repr__',
 '__setattr__',
 '__setstate__',
 '__sizeof__',
 '__str__',
 '__subclasshook__',
 '__weakref__',
 '_angular_trees',
 '_dist_args',
 '_distance_correction',
 '_distance_func',
 '_init_search_function',
 '_init_search_graph',
 '_init_sparse_search_function',
 '_is_sparse',
 '_original_num_threads',
 '_raw_data',
 '_search_forest',
 '_search_function',
 '_search_graph',
 '_tree_search',
 '_vertex_order',
 '_visited',
 'compress_index',
 'compressed',
 'delta',
 'dim',
 'diversify_prob',
 'leaf_size',
 'low_memory',
 'max_candidates',
 'metric',
 'metric_kwds',
 'n_iters',
 'n_jobs',
 'n_neighbors',
 'n_search_trees',
 'n_trees',
 'neighbor_graph',
 'prepare',
 'prune_degree_multiplier',
 'query',
 'random_state',
 'rng_state',
 'search_rng_state',
 'tree_init',
 'update',
 'verbose']
>>> index.update?
>>> index.update??
>>> np.array(list(range(300)))
array([  0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,
        13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,
        26,  27,  28,  29,  30,  31,  32,  33,  34,  35,  36,  37,  38,
        39,  40,  41,  42,  43,  44,  45,  46,  47,  48,  49,  50,  51,
        52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,  64,
        65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,
        78,  79,  80,  81,  82,  83,  84,  85,  86,  87,  88,  89,  90,
        91,  92,  93,  94,  95,  96,  97,  98,  99, 100, 101, 102, 103,
       104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116,
       117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129,
       130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142,
       143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155,
       156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168,
       169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181,
       182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194,
       195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207,
       208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220,
       221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233,
       234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246,
       247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259,
       260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272,
       273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285,
       286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298,
       299])
>>> v = _
>>> v.reshape(1, -1)
array([[  0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,
         13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,
         26,  27,  28,  29,  30,  31,  32,  33,  34,  35,  36,  37,  38,
         39,  40,  41,  42,  43,  44,  45,  46,  47,  48,  49,  50,  51,
         52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,  64,
         65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,
         78,  79,  80,  81,  82,  83,  84,  85,  86,  87,  88,  89,  90,
         91,  92,  93,  94,  95,  96,  97,  98,  99, 100, 101, 102, 103,
        104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116,
        117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129,
        130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142,
        143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155,
        156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168,
        169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181,
        182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194,
        195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207,
        208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220,
        221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233,
        234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246,
        247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259,
        260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272,
        273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285,
        286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298,
        299]])
>>> index.update??
>>> newvecs = load_hdf5(num_vecs=2000000)
>>> len(newvecs)
2
>>> len(newvecs[0])
999994
>>> len(newvecs[1])
999994
>>> newvecs[0][0]
array([ 1.0730e-01,  8.9000e-03,  6.0000e-04,  5.5000e-03, -6.4600e-02,
       -6.0000e-02,  4.5000e-02, -1.3300e-02, -3.5700e-02,  4.3000e-02,
       -3.5600e-02, -3.2000e-03,  7.3000e-03, -1.0000e-04,  2.5800e-02,
       -1.6600e-02,  7.5000e-03,  6.8600e-02,  3.9200e-02,  7.5300e-02,
        1.1500e-02, -8.7000e-03,  4.2100e-02,  2.6500e-02, -6.0100e-02,
        2.4200e-01,  1.9900e-02, -7.3900e-02, -3.1000e-03, -2.6300e-02,
       -6.2000e-03,  1.6800e-02, -3.5700e-02, -2.4900e-02,  1.9000e-02,
       -1.8400e-02, -5.3700e-02,  1.4200e-01,  6.0000e-02,  2.2600e-02,
       -3.8000e-03, -6.7500e-02, -3.6000e-03, -8.0000e-03,  5.7000e-02,
        2.0800e-02,  2.2300e-02, -2.5600e-02, -1.5300e-02,  2.2000e-03,
       -4.8200e-02,  1.3100e-02, -6.0160e-01, -8.8000e-03,  1.0600e-02,
        2.2900e-02,  3.3600e-02,  7.1000e-03,  8.8700e-02,  2.3700e-02,
       -2.9000e-02, -4.0500e-02, -1.2500e-02,  1.4700e-02,  4.7500e-02,
        6.4700e-02,  4.7400e-02,  1.9900e-02,  4.0800e-02,  3.2200e-02,
        3.6000e-03,  3.5000e-02, -7.2300e-02, -3.0500e-02,  1.8400e-02,
       -2.6000e-03,  2.4000e-02, -1.6000e-02, -3.0800e-02,  4.3400e-02,
        1.4700e-02, -4.5700e-02, -2.6700e-02, -1.7030e-01, -9.9000e-03,
        4.1700e-02,  2.3500e-02, -2.6000e-02, -1.5190e-01, -1.1600e-02,
       -3.0600e-02, -4.1300e-02,  3.3000e-02,  7.2300e-02,  3.6500e-02,
       -1.0000e-04,  4.2000e-03,  3.4600e-02,  2.7700e-02, -3.0500e-02,
        7.8400e-02, -4.0400e-02,  1.8700e-02, -2.2500e-02, -2.0600e-02,
       -1.7900e-02, -2.4280e-01,  6.6900e-02,  5.2300e-02,  5.2700e-02,
        1.4900e-02, -7.0800e-02, -9.8700e-02,  2.6300e-02, -6.1100e-02,
        3.0200e-02,  2.1600e-02,  3.1300e-02, -1.4000e-02, -2.4950e-01,
       -3.4600e-02, -4.8000e-02,  2.5000e-02,  2.1300e-01, -3.3000e-02,
       -1.5530e-01, -2.9200e-02, -3.4600e-02,  1.0740e-01,  1.0000e-03,
       -1.1700e-02, -5.7000e-03, -1.2800e-01, -3.8000e-03,  1.3000e-02,
       -1.1570e-01, -1.0800e-02,  2.7500e-02,  1.5800e-02, -1.6900e-02,
        7.0000e-03,  2.4700e-02,  5.1000e-02,  1.0292e+00, -2.8300e-02,
       -3.1000e-02, -2.6000e-03, -3.4300e-02,  5.7800e-02,  4.4400e-02,
        8.1200e-02, -2.1100e-02, -8.7200e-02,  1.6900e-02,  4.9900e-02,
        4.8500e-02,  2.2700e-02, -3.2300e-02, -3.5000e-03,  4.3500e-02,
       -2.7500e-02,  1.5400e-02,  1.3500e-02, -4.8400e-02, -6.9900e-02,
       -5.0200e-02,  2.7450e-01, -3.0000e-04, -3.7100e-02,  5.1700e-02,
       -9.0800e-02,  1.3000e-03,  3.6000e-02,  2.8000e-02,  8.3900e-02,
        9.8000e-02, -4.9000e-02, -2.4230e-01, -1.4200e-02,  2.4000e-03,
       -2.0700e-02,  1.2000e-03,  8.8000e-03, -1.4300e-02, -1.9700e-02,
        5.1500e-02, -8.5000e-03,  2.5700e-02,  2.1540e-01,  3.0100e-02,
        2.1100e-02,  5.3000e-02, -5.0000e-04,  1.7700e-02,  1.6000e-03,
       -5.3000e-03, -1.6200e-02, -2.2300e-02, -1.8620e-01,  3.9800e-02,
        6.5800e-02, -9.6200e-02, -7.6000e-03, -7.5000e-03, -3.4200e-02,
       -2.6500e-02,  4.2000e-02,  5.2200e-02, -2.6600e-02,  2.0100e-02,
       -1.3310e-01, -3.6700e-02,  3.5100e-02,  5.1800e-02, -8.7000e-03,
        5.9900e-02, -1.0860e-01, -1.8800e-02,  4.8100e-02,  1.0500e-02,
       -6.0000e-03,  1.5100e-02, -3.1000e-03,  7.7000e-03, -2.7600e-02,
       -3.7300e-02, -2.0300e-02,  4.7200e-02,  2.4600e-02,  1.4400e-01,
        5.4200e-02, -2.2500e-02,  2.4950e-01,  1.6170e-01,  3.8000e-03,
        1.1190e-01, -2.3000e-02, -7.8500e-02,  2.5000e-02, -6.1600e-02,
       -4.8500e-02,  2.2500e-02,  2.8100e-02,  4.1000e-03,  1.1200e-02,
        1.7200e-02,  2.9100e-02, -2.8200e-02,  2.6000e-03,  4.0550e-01,
        3.9200e-02,  8.8000e-03,  2.2800e-02,  2.9900e-02,  1.1950e-01,
        5.4500e-02, -2.0000e-03,  2.0000e-03,  4.9000e-02,  1.4500e-02,
       -8.6000e-03,  9.8000e-03, -2.3600e-02,  1.7100e-02, -7.6500e-02,
       -4.0000e-02,  1.2800e-02,  1.1000e-03,  4.2000e-03,  2.4400e-02,
        7.5000e-03,  2.0000e-02,  2.0100e-02,  1.9600e-02, -3.7700e-02,
       -4.3200e-02, -7.3000e-03, -2.1000e-03,  1.8300e-02,  7.6000e-03,
        1.8050e-01, -5.5100e-02,  7.5000e-03, -5.1600e-02,  4.2000e-02,
       -6.8000e-03, -7.1100e-02, -1.4080e-01,  5.0400e-02,  2.7600e-02,
        4.7000e-02,  3.2300e-02, -2.1900e-02,  1.0000e-03,  8.9000e-03,
        2.7600e-02,  1.8600e-02,  5.0000e-03,  1.1730e-01, -4.0000e-02],
      dtype=float32)
>>> vecs2, vocab2 = newvecs
>>> vecs2[0] == vecs[0]
array([ True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True,  True,  True,  True,  True,  True,  True,
        True,  True,  True])
>>> len(vecs)
100000
>>> len(vecs[99_999])
300
>>> len(vecs[-1])
300
>>> all(vecs[-1] == vecs2[99_999])
True
>>> vecs2 = vecs2[100_000:]
>>> vocab2 = vocab2[100_000:]
>>> index.update(vecs2)
>>> dir(index)
['__class__',
 '__delattr__',
 '__dict__',
 '__dir__',
 '__doc__',
 '__eq__',
 '__format__',
 '__ge__',
 '__getattribute__',
 '__getstate__',
 '__gt__',
 '__hash__',
 '__init__',
 '__init_subclass__',
 '__le__',
 '__lt__',
 '__module__',
 '__ne__',
 '__new__',
 '__reduce__',
 '__reduce_ex__',
 '__repr__',
 '__setattr__',
 '__setstate__',
 '__sizeof__',
 '__str__',
 '__subclasshook__',
 '__weakref__',
 '_angular_trees',
 '_dist_args',
 '_distance_correction',
 '_distance_func',
 '_init_search_function',
 '_init_search_graph',
 '_init_sparse_search_function',
 '_is_sparse',
 '_original_num_threads',
 '_raw_data',
 '_rp_forest',
 '_search_forest',
 '_search_function',
 '_search_graph',
 '_tree_search',
 '_vertex_order',
 '_visited',
 'compress_index',
 'compressed',
 'delta',
 'dim',
 'diversify_prob',
 'leaf_size',
 'low_memory',
 'max_candidates',
 'metric',
 'metric_kwds',
 'n_iters',
 'n_jobs',
 'n_neighbors',
 'n_search_trees',
 'n_trees',
 'neighbor_graph',
 'prepare',
 'prune_degree_multiplier',
 'query',
 'random_state',
 'rng_state',
 'search_rng_state',
 'tree_init',
 'update',
 'verbose']
>>> dir(index.neighbor_graph)
['__bool__',
 '__class__',
 '__delattr__',
 '__dir__',
 '__doc__',
 '__eq__',
 '__format__',
 '__ge__',
 '__getattribute__',
 '__gt__',
 '__hash__',
 '__init__',
 '__init_subclass__',
 '__le__',
 '__lt__',
 '__ne__',
 '__new__',
 '__reduce__',
 '__reduce_ex__',
 '__repr__',
 '__setattr__',
 '__sizeof__',
 '__str__',
 '__subclasshook__']
>>> index._neighbor_graph = index.neighbor_graph
>>> dir(index)
['__class__',
 '__delattr__',
 '__dict__',
 '__dir__',
 '__doc__',
 '__eq__',
 '__format__',
 '__ge__',
 '__getattribute__',
 '__getstate__',
 '__gt__',
 '__hash__',
 '__init__',
 '__init_subclass__',
 '__le__',
 '__lt__',
 '__module__',
 '__ne__',
 '__new__',
 '__reduce__',
 '__reduce_ex__',
 '__repr__',
 '__setattr__',
 '__setstate__',
 '__sizeof__',
 '__str__',
 '__subclasshook__',
 '__weakref__',
 '_angular_trees',
 '_dist_args',
 '_distance_correction',
 '_distance_func',
 '_init_search_function',
 '_init_search_graph',
 '_init_sparse_search_function',
 '_is_sparse',
 '_neighbor_graph',
 '_original_num_threads',
 '_raw_data',
 '_rp_forest',
 '_search_forest',
 '_search_function',
 '_search_graph',
 '_tree_search',
 '_vertex_order',
 '_visited',
 'compress_index',
 'compressed',
 'delta',
 'dim',
 'diversify_prob',
 'leaf_size',
 'low_memory',
 'max_candidates',
 'metric',
 'metric_kwds',
 'n_iters',
 'n_jobs',
 'n_neighbors',
 'n_search_trees',
 'n_trees',
 'neighbor_graph',
 'prepare',
 'prune_degree_multiplier',
 'query',
 'random_state',
 'rng_state',
 'search_rng_state',
 'tree_init',
 'update',
 'verbose']
>>> [v for v in dir(index) if 'com' in v]
['compress_index', 'compressed']
>>> index.compressed
True
>>> [v for v in dir(index) if 'x' in v]
['__reduce_ex__', '_vertex_order', 'compress_index', 'max_candidates']
>>> [v for v in dir(index) if 'gr' in v]
['_init_search_graph',
 '_neighbor_graph',
 '_search_graph',
 'neighbor_graph',
 'prune_degree_multiplier']
>>> index._init_search_graph()
>>> Index??
>>> NNDescent??
>>> pynn.NNDescent??
>>> index = Index(vecs, vocab=vocab, compressed=False)
>>> index = Index??
>>> index = Index(vecs, metric="cosine", compressed=False)
>>> index.update(vecs2)
>>> index = pynn.NNDescent(vecs, vocab=vocab, compressed=False)
>>> index = pynn.NNDescent(vecs, metric='cosine', compressed=False)
>>> index.update(vecs2, compressed=False)
>>> index.update??
>>> index.update
<bound method NNDescent.update of <pynndescent.pynndescent_.NNDescent object at 0x7fe61d25cd60>>
>>> index.update(vecs2)
>>> __version__
>>> who
>>> pynn.__version__
'0.5.6'
