# Logging

Python's logging framework is great for testing of web services or background processes when you don't have access to the terminal.

But logging usually sends error and warning messages to a file on the server where the Python process is running.
So for production web services it's better to use an external logging service such as PaperTrail (by Solar Winds) or Sentry.

But the free accounts on Sentry and PaperTrail usually run out of storage/transfer limits.
So we've disabled it for our Team on Render.com.
If you need to add it back, go to [Team Settings](https://dashboard.render.com/t/settings) on your Render Dashboard, or your [personal Render account settings](https://dashboard.render.com/u/settings), and scroll down to ["Log Streams" section](https://dashboard.render.com/t/tea-/settings#log-streams).

Sentry is probably your best bet for getting up to speed quickly on logging web services.
They have decent limits for their free tier (pricing).
And they have code snippets with your tokens ready to go for pasting into Javascript or Python.
Of course you'll probably want to protect your tokens.
