# Designing a chatbot Proof of Concept.
A nonprofit came to you and asked to design a chatbot for them. In this exercise, you will be creating a simple prototype, or Proof of Concept, of a working chatbot. 

## Project summary 

**Client**: Tai Tanzania ([https://tai.or.tz/](https://tai.or.tz/)) is a Tanzanian organization that promotes social and behavioral change in Tanzania using 3-D videos like [this one](https://www.youtube.com/watch?v=k5CDQwvHUtY). The organization's operational mode currently is to screen the video in in-person class sessions, and then have a discussion with students about the topics discuss in videos. They now want to engage more teens and scale their impact nationally through social media and chatbots. 

**Client's Goals**: Tai Tanzania aims to achieve a few goals with the chatbot: 
* Engage the teens after they watch the educational videos and help them process and learn from the videos. 
* Collect stories from the teens, that relate to the situations described in the video. 
* Collect questions from the teens about sexual education topics that are of interest to them.  

## Step 1: Understand your client and your user
* Browse the client's website, and understand better what they do. Watch some of the 3-D videos they use. 
* Think about the target audience. Who is the typical user of the bot? Why they might be interested in interacting with it? 
* Imagine the user's journey with the bot. Where will they find the bot? How will they start the conversation? 
* What would a successful conversation look like? In the commercial sector, a succesful chatbot conversation typically ends either with "conversion" - the user leaving their email, or purchasing the product; or with the user's problem being resolved. What would be a successfult outcome for this bot's case?

## Step 2: Plan the bot's functionality
Now you understand better who are you building the chatbot for, and what problem you are trying to solve. 
Time to dig into what your chatbot will actually do! 

* Brainstorm 5-10 things that your bot can do in its conversation with the user. Use action verbs, such as "answer", "collect", "quiz", "help" etc. 
* Which of these feautures are the most important ones? Choose 2-4 that you think are the most impactful. 

## Step 3: Write happy conversation
A "Happy conversation" is a script of the bot's conversation with the user if everything goes well. 
Quite often, we would co-create this conversation with the client - for this exercise, use your imagination.
Because this is a prototype and not a full bot, limit your "happy conversations" to up to 5 bot turns and 5 user turns - 10 turns in total.

You can use a simple doc or markdown file for this step. Write the "happy conversation" that will involve one or more of the chatbot's features you did in the previous step. 

There might well be more than one happy path for your bot! Keep writing them until all the important functionality in step 2 is covered. 

## Step 4: Plan the chatbot outline
Now you want to dive into the structure of your bot. Use draw.io, PowerPoint or just a piece of paper to create a flowchart of how different pieces of the bot connect between themselves. 

These pieces are often called "flows" in chatbot lingo. You can think about flows like the analog of functions in programming languages. They can contain one or two exchanges between the bot and the user, and can contain 20. It's their functionality that matters.  

* Think about the flows your bot should have. In addition to the flows that contain your important features, all bots have a welcome message and a good-bye message, and most have some form of "main menu". 
* Build a flowchart of the flows that you thought of by connecting the flows in the way that make sense. 

## Step 5: Build your prototype! 




