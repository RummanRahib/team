# Robust NLP Systems
Tangible AI Internship Project by Kazuma Endo\
Link: https://gitlab.com/kazumaendo/fake_vs_real_news
## Summary
1. Create fake news prediction model without leakage
2. Adversarial synonym substitution
3. Robustness measurement
4. Beating the Fake News Prediction model. Random search to find best set of hyperparameter to reduce accuracy the most.
5. Modify model to make it more robust in a general sense. How to make nlp models more robust

## Table of Contents
  - [Introduction](#introduction)
  - [Data](#data)
  - [Robust NLP](#robust-nlp)
  - [Results](#results)
  - [Discussion](#discussion)
  - [Conclusion](#conclusion)
  - [References](#references)


## Introduction
Current state-of-the-art NLP systems can achieve high accuracy in many domains they are applied in, such as machine translation, sentiment analysis, Q&A system, and text summarization. However, although they have high accuracy when tested on common benchmark datasets, these systems perform poorly when given inputs in an unexpected form. In other words, these models are brittle. This problem seems to arise since these models rely too much on the datasets they are trained on and assume that the datasets are drawn from roughly the same distribution. This causes further problems as it raises the question of whether the performance measures outputted by benchmark datasets are valid or not. In this project, we reduced the model accuracy by 50% by substituting synonyms of the input texts. Since the world around these systems is constantly changing, such as shifts in contexts and frequencies of certain words used over time, it is a problem if these systems get deployed to the real world without considering their robustness. In addition, taking into account that there are adversary systems or persons deliberately trying to fool the NLP system, it makes it even more valuable to consider the robustness of such models. You can imagine how important it is for spam and phishing filters to deal with clever adversaries robustly.

This project looks at the Fake News Prediction Problem, as a toy problem, and examines how to measure the robustness of the model and search for techniques to make NLP models more robust. This toy problem relies heavily on the data collected, as the data used in this model were taken during Trump vs. Clinton election, and thus makes it a good candidate for looking at model brittleness. In addition, the articles were collected from 5 news sources. And all the articles from 3 of the sources were labeled `fake`. All the articles from the other news sources were labeled `not_fake`. From Kaggle, there are many cases where amateur data scientists achieved accuracy of over 99%, suggesting that there is a problem with robustness and possible data leakage. We dealt with the data leakage before diving into tackling the robustness problem to make the NLP problem harder and more generalizable. Without a reasonably difficult problem, the model would rely on the dataset to make predictions and not the underlying meaning of the text. 

## Data
Fake News Dataset: https://www.kaggle.com/clmentbisaillon/fake-and-real-news-dataset/download 
- Dataset that contains labeled real and fake news articles

Spacy Language Models: https://spacy.io/models/en 
- Used to measure word similarity, lemmatizing words, finding the part of speech of the word, and creating word embedding vector of texts

SBERT Dataset: https://www.sbert.net/
- Used to create word embedding vector of texts

NLTK Dataset: https://www.nltk.org/_modules/nltk/corpus/reader/wordnet.html 
- Used to list of possible synonyms of a certain word. 

sklearn TFIDF Vectorizer: https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html

sklearn SVC: https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html

## Robust NLP
The robustness of a model is its ability to work well even when presented with unexpected inputs. For the Fake News Prediction model benchmark we used only the article title as input, and the target variable remained `is_fake`. To measure the robustness, we needed a way to create adversarial examples by substituting synonyms from the article titles. With this synonym substitution function implemented, the model accuracy difference between the original and the synonym substituted title, the expected vs. the unexpected format, was calculated to get the numerical representation of robustness.\
**Example of synonym substitution:**

|       | Title |
| ----------- | ----------- |
| Original      | Trump picks former U.S. Senator Coats as director of national intelligence       |
| Substituted   | Trump picks old America Senator Coats as manager of national intelligence       |

The goal of synonym substitution was to reduce machine accuracy while maintaining human accuracy on the task of labeling the news as real or fake based on the article title. By achieving this label preserving perturbation, we were able to use the accuracy difference of the model with original vs. synonym substituted inputs as the robustness score since the model should have predicted the article in the same manner in either case. This was a crucial feature that our synonym substituter had to have since if the perturbed title fools humans, it is not feasible to expect the machine to give a correct prediction.

To find the best set of hyperparameters to use in the process of synonym substitution, we used a random search algorithm that went through different combinations of allowed "parts of speech" of the word to modify. The confidence of the model to correctly classify the article was calculated for each of the possible word substitutions given by WordNet with NLTK. Then we chose the word that reduced model confidence the most as the synonym to substitute with, achieving adversarial synonym substitution. Finally, the hyperparameter was reviewed by a human to see if the synonym substitution is a label preserving perturbation. In this project, the part of speech "ADJ, ADV, NOUN, PROPN" was used when substituting synonyms of article titles. By using this for substituting synonyms, out of the 500 test dataset that we used (out of 8980 total) to calculate robustness of models, only 1 title went unchanged. The total runtime for finding synonym substituted titles for 500 data was 7:30 minutes, roughly 1 second per title. 

|    Part of Speech       |  Reduction in Accuracy |
| ----------- | ----------- |
| ADJ, ADV, NOUN, PROPN | -0.519       |
| ADJ, NOUN, PROPN | -0.506     |
| ADV, NOUN, PROPN | -0.466       |
| ADJ, ADV, PROPN | -0.334      |
| ADV, PROPN | -0.268      |
| ADJ, NOUN | -0.264       |
| ADV, NOUN | -0.230      |
| NOUN | -0.224       |
| ADV | -0.053       |

Result of Random Search for Synonym Substitution on TFIDF Linear model (max_df = 0.7, min_df = 7). VERB was omitted from Part of Speech since it did not satisfy label preserving perturbation

```
def accuracy_diff(X_test1, X_test2, y_test, pipe):
    y_pred1 = pipe.predict(X_test1)
    accuracy1 = accuracy_score(y_test, y_pred1)  
    y_pred2 = pipe.predict(X_test2)
    accuracy2 = accuracy_score(y_test, y_pred2)
    acc_diff = accuracy2 - accuracy1
    return acc_diff
```
How Reduction in Accuracy is calculated
## Results

**Robustness Hyperparameter**
|  Feature Engineering  | SVC Kernel     |   max_df (%) |   min_df (#) |   Vector Size |   Test Accuracy |   Train Accuracy |   Reduction in Accuracy |   Robust Test Accuracy |
|---:|:---------------|-------------:|-------------:|--------------:|----------------:|-----------------:|-------------------:|-----------------------:|
| SBERT_2  | Poly deg:3  | n/a | n/a | 768 |        0.968 |         0.993 |          -0.106 |               0.862 |
| SBERT_2  | Poly deg:5  | n/a | n/a | 768 |        0.960 |         0.998 |          -0.102 |               0.858 |
| Spacy_lg  | Poly deg:7  | n/a | n/a | 300 |        0.963 |         0.985 |          -0.114 |               0.848 |
|  Spacy_lg | Poly deg:5  | n/a | n/a | 300 |        0.963 |         0.981 |          -0.114 |               0.848 |
| SBERT_2  | Linear  | n/a | n/a | 768 |        0.952 |         0.953 |          -0.110 |               0.842 |
| SBERT_1  | Poly deg:3  | n/a | n/a | 384 |        0.946 |         0.989 |          -0.108 |               0.837 |
| Spacy_lg  | Poly deg:3  | n/a | n/a | 300 |        0.958 |         0.970 |          -0.122 |               0.836 |
|  Spacy_lg | RBF            | n/a | n/a | 300 |        0.960 |         0.966 |          -0.124 |               0.836 |
| SBERT_1  | Linear  | n/a | n/a | 384 |        0.935 |         0.938 |          -0.108 |               0.826 |
| Spacy_lg  | Poly deg:10 | n/a | n/a | 300 |        0.958 |         0.983 |          -0.140 |               0.818 |
|  Spacy_lg | Linear         | n/a | n/a | 300 |        0.941 |         0.945 |          -0.136 |               0.805 |
|  Spacy_md | Linear         | n/a | n/a | 300 |        0.930  |         0.934 |          -0.158 |               0.771 |
|  Spacy_lg | Sigmoid        | n/a | n/a | 300 |        0.872 |         0.867 |          -0.128 |               0.743 |
|  Spacy_lg | Poly deg:15 | n/a | n/a | 300 |        0.898 |         0.935 |          -0.166 |               0.732 |
| SBERT_1  | Poly deg:5  | n/a | n/a | 384 |        0.874 |         0.982 |          -0.154 |               0.720 |
| TFIDF | Poly deg:7  |          0.7 |           30 |          1910 |        0.774 |         0.999 |          -0.120  |               0.654 |
| TFIDF | Poly deg:5  |          0.7 |           30 |          1910 |        0.918 |         0.999 |          -0.271 |               0.648 |
| TFIDF | Poly deg:3  |          0.7 |           30 |          1910 |        0.928  |         0.997 |          -0.343 |               0.585 |
| TFIDF | Poly deg:10 |          0.7 |           30 |          1910 |        0.686 |         0.999 |          -0.126 |               0.559 |
| TFIDF  | Linear         |          0.7 |          500 |            57 |        0.713  |         0.715 |          -0.166 |               0.547 |
| TFIDF  | Linear         |          0.7 |          120 |           516 |        0.847 |         0.860 |          -0.313 |               0.534 |
| TFIDF | Poly deg:15 |          0.7 |           30 |          1910 |        0.676 |         0.999 |          -0.144 |               0.532 |
| TFIDF  | RBF            |          0.7 |           30 |          1910 |        0.924 |         0.981 |          -0.415  |               0.509 |
|  TFIDF | Linear         |          0.7 |           60 |          1061 |        0.888 |         0.899 |          -0.381 |               0.507   |
| TFIDF  | Linear         |          0.7 |           30 |          1910 |        0.908 |         0.924 |          -0.403 |               0.505 |
| TFIDF | Sigmoid        |          0.7 |           30 |          1910 |        0.897 |         0.889 |          -0.403 |               0.494 |
| TFIDF  | Linear         |          0.7 |           15 |          3019 |        0.915 |         0.937 |          -0.467 |               0.448 |
| TFIDF  | Linear         |          1   |            0 |         15666 |        0.926 |         0.962 |          -0.503 |               0.423 |
|  TFIDF | Linear         |          0.7 |            0 |         15666 |        0.926 |         0.962 |          -0.503 |               0.423 |
| TFIDF  | Linear         |          0.7 |            7 |          4514 |        0.918 |         0.945  |          -0.519 |               0.399 |
| TFIDF  | Linear         |          0.6 |            7 |          4514 |        0.920 |         0.946 |          -0.523 |               0.396 |

**Feature Engineering Info**
|  Feature Engineering  | Pre-trained Model     |   Dimensions |   Size(MB) |
|---:|:---------------|-------------:|-------------:|
| SBERT_2  | all-mpnet-base-v2 | 768 | 418 | 
| SBERT_1  | all-MiniLM-L6-v2 | 384 | 80| 
| Spacy_lg  | en_core_web_lg | 300 | 741| 
| Spacy_md  | en_core_web_md | 300 | 43| 

Figure A             |   Figure B
:-------------------------:|:-------------------------:
![Screen_Shot_2021-09-04_at_11.41.36_AM](/uploads/12228382a76b1b8e47d1759dc5988e3c/Screen_Shot_2021-09-04_at_11.41.36_AM.png)   |  ![Screen_Shot_2021-09-04_at_11.41.25_AM](/uploads/b9fe81ec23debde322b776b2c307914c/Screen_Shot_2021-09-04_at_11.41.25_AM.png)

&nbsp;

From the results, we believe it is important to separately analyze how different variables affect the model individually.
 1. Difference between distinct feature engineering methods 
 2. Difference within TFIDF linear model with different TFIDF parameters
 3. Difference between different SVC kernels
   
There is a notable difference in the effect that the different feature engineering methods have on the robustness of the model. The three methods that we used in this project were TFIDF, Spacy, and SBERT. TFIDF quantifies textual data by calculating the relevancy of a word, whereas Spacy and SBERT quantify them by using word embeddings from pre-trained models. We can observe that, overall, the best method to use is SBERT, with Spacy as a close second, and TFIDF being the least favorable. We believe that the robustness of a model when using TFIDF is much lower than the other methods because TFIDF only considers information at the word level, causing the model to rely too heavily on individual words that only appear in the training dataset. In contrast, Spacy and SBERT take into account the whole sentence. This sentence embedding helps the machine understand or retain the information about context, intention, and other nuances in the textual data. With this, we found that when a model only considers information at the word level, it could be losing key information attained when considering the entire sentence and its semantic information, allowing for a more robust interpretation of the text. Between sentence embedding methods, SBERT performs better than Spacy since it is the state-of-the-art text embedding system. Furthermore, within Spacy and SBERT, we can see differences between the different pre-trained models. However, this difference comes at a cost since more robust models require more memory and are thus slower to train.

When only considering the effects different TFIDF parameters have on the model, we observe that a higher min_df value causes the model to become more robust. We believe this is the case because a higher min_df value reduces the vector size. With this, the model becomes less reliant on the dataset that they are trained and evaluated on, minimizing the accuracy reduction. However, the test accuracy decreases in parallel, and thus the robust test accuracy stabilizes near 54% which is not significantly better than simply taking a guess. This is represented by the red line in Figures 1.1 and 1.2, where higher test accuracy is associated with low min_df value and lower test accuracy with high min_df. 

It was interesting to find that the polynomial kernel performs better than the linear kernel since we thought it would cause the model to rely more on the dataset. Upon further examination, the polynomial degree played a big part in the accuracy and the robustness of the model since picking a degree that is too high would cause the model to overfit (Figure 1.1 & 1.2). But this optimal degree of the polynomial kernel seemed to vary for the different feature engineering methods, so it must be taken with care. Furthermore, a higher polynomial degree came at a cost of a slower training speed.  

With these in mind, we believe that feature engineering is one of the most important steps to consider when constructing a robust model. Better representation of raw data as numerical values leads to a more flexible and simpler model that is more robust and generalizable. 

## Discussion
The robustness of a model is an important measure that represents the quality of the model. It is important to consider this metric alongside other model's quality measurements such as performance and memory. An example of this can be seen in the comparison between en_core_web_lg and en_core_web_md in Spacy vectorization or between the two SBERT datasets. The larger pre-trained model with a bigger size has better robustness but worse memory and slower training speed. If possible, it would be best to make a model that has the best of all model quality measurements, but it is important to think about which one of the attributes is most relevant to the problem that the model is applied in and reallocating the weights between the attributes to find the optimal model. In this project, we thought robustness should be weighed more than memory, and thus concluded the larger dataset as the optimal dataset.

We believe that the findings of this project can be applied to the real world in variety of domains such as machine translation, sentiment analysis, Q&A system, and text summarization. Furthermore, we believe that robustness needs to be expanded for models to deal with cultural differences of the real world. From a social perspective, models must have similar performance to underrepresented groups of people as they would to the majority. This calls for further research since underrepresented groups will have less data than the majority, making it easy for the model to cheat and do well on the majority while doing poorly on the minority. While these problems will likely not have an obvious answer, it must be tackled since it has a direct effect on social good.

## Conclusion
NLP systems are relied on more than ever as they have become a part of everyday human lives. With that said, it is our responsibility to make sure that errors concerning robustness are kept at a minimum. We were able to achieve a more robust model and identify the factors that contribute to such robustness. However, further research needs to be done since many perturbations could happen to a natural language problem. 

Throughout this project, I was able to build intuition as to what hyperparameter and model architecture to choose to build a more robust model. One of the main focuses of this project was to create a general model for language that works for a long time for a large number of people, topics, and contexts and we believe we were able to take one big step towards that. The other focus of this project was to construct a way to measure the robustness and with this, I learned the skill of testing any ideas I have about building a better model for any data science problem. Moreover, this project made me realize that it is easy to 'cheat' when constructing models. For example, we could have specialized the modeling approach to work well only on this problem, to be robust only to synonym substitution using WordNet with NLTK, by using active learning to increase training set with synonym substitution. However, this will only cause the model to be good at predicting inputs that are within the domain of the particular training set and the synonym substitution set, not robust in the general sense. 

Hopefully, in the coming years, the intuitions and the skills I learned would help me achieve the ultimate goal: making fair and error-prone machine learning models.

## References
**TangibleAI** \
https://tangibleai.com/

**Robin Jia Thesis** \
https://robinjia.github.io/assets/pdf/robinjia_thesis.pdf

**Stanford Keynote Talk by Christopher Potts** \
https://www.youtube.com/watch?v=t_A36DDcG_0

**Robust, Unbiased Natural Language Processing by Timothy Baldwin** \
https://people.eng.unimelb.edu.au/tbaldwin/pubs/rep4nlp2018-bias.pdf
