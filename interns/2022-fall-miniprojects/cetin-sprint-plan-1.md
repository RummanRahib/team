# Cetin Test Project Sprint 1

- [X] C1: clone nudger repo
- [X] C1: branch feature-faq
- [X] H1: link to Greg's tutorial on setting up a new django project
- [X] C1: get environment variables (.env file)
- [X] C2: get nudger app working (install dependencies)
- [X] C1: load nudger data into database
- [X] C1: create new django app called "faqbot"
- [X] C2: create a FAQ model in models.py
- [X] C1: modify the data loading script to add one QA pair - Who are you? - 
- [X] C1: modify data loading script to load a CSV file with 2 columns Question, Answer
- [X] C1: create endpoint that takes {"question": Who are you?} and returns {"question": Who are you?, answer: I'm Qary!}` 
- [X] C1: add unittest with the input-output above
- [X] C1: merge request for all these features for the nudger app
