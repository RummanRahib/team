# Sprint 1

## Project Description
**Project:** "Django-Delvin"

**Implementer:** Alex

**Project Description:**

Delvin is an analytics platform that ingests message data from chatbots to help nonprofit organizations analyze the activity happening in their chatbot.  We have incorporated several platforms into the project, including Landbot, TextIt, and Turn.io.  We would like to incorporate more platforms, including Telegram.


## Current Sprint

**Sprint Goal**

**Goal 1:** Now that we have the ability to receive data from Turn.io, Landbot, and (next task) Telegram.  We would like to be able to save the data based on our [model schema](https://gitlab.com/tangibleai/django-delvin/-/blob/feature-pulling-chatbot-message/message/models.py).

**Goal 2:** Delvin needs to work with many platforms to meet users where they are at.  We would like Delvin to be able to receive message data from a Telegram chatbot/chatroom and, ultimately, store it in a database.

**Current Sprint Tasks:**
- [ ] A - : Issue 2 (high priority - by early next week is best)
  * Make sure that the Landbot integration is working, and that you can receive the data object from the messagehook.
  * Review the [Landbot Message Hook documentation](https://gitlab.com/tangibleai/django-delvin/-/blob/main/docs/landbot-webhook.md) to understand the context of this function.
  * Use this Landbot chatbot to test your fix.  [Sample Landbot](https://landbot.pro/v3/H-1421823-ZGLC9V27V1SPTS1B/index.html)


- [ ] A - : Understand the data schema we want all the integrations to follow
  * Review the model fields we need from [models.py](https://gitlab.com/tangibleai/django-delvin/-/blob/feature-pulling-chatbot-message/message/models.py)
    - Project = Chatbot project's name
    - Contact = user
    - Message = Chatbot message interaction content

- [ ] A - : Each integration should have data objects for all 3 models (ie., project_data, contact_data, message_data)
  * Landbot
  * Turn.io
  * Telegram

- [ ] A - : Create (or reuse) a simple Telegram bot
  * Make a simple Telegram bot with a few exchanges

- [ ] A - : Make Django-Delvin collect the relevant data from a Telegram message
  * You might find this [quick tutorial](https://betterprogramming.pub/how-to-get-data-from-telegram-82af55268a4b) helpful
  * Django-Delvin should be able to receive data from the Telegram chatbot.
  * Filter the data object from Telegram to get the information we need in a JSON object/dictionary






## Past Sprints
### **Sprint 1**

**Sprint Goal**

There are two issues with platform intergrations that need to be fixed in the Django-delvin application.

**Completed Sprint Tasks**
- Preparation Steps
  * [x] Set up an account on Gitlab
  * [x] Send Greg your Gitlab id.
  * [x] Set up an account on [Render.com](https://render.com/)
  * [x] Review the [Django Project Process document](https://gitlab.com/tangibleai/team/-/blob/main/exercises/resources/django-project-processes.md) to understand the project architectures and processes we are working towards
  * [x] Give Alex access to the [Django-Delvin repository](https://gitlab.com/tangibleai/django-delvin) (after getting his Gitlab id).
  * [x] Clone the project, and work on the [feature-platform-connections branch](https://gitlab.com/tangibleai/django-delvin/-/tree/feature-platform-connections).
  
- Issue 1

  * On line 130, there is a problem accessing the 'messages' content in the `data_json` object we get from Turn.io.  While the integration still seems to be processing requests correctly, we don't want to receive this error message.
    ```
    File "/opt/render/project/src/maitag/platforms/turn.py", line 130, in label_and_resubmit_message
    Nov 17 01:41:41 PM      message = data_json['messages'][0]['text']['body']
    Nov 17 01:41:41 PM  KeyError: 'messages'
    ```
  * [x] Review the `intent-recognition-overview.md` in `django-delvin/docs`:  [link](https://gitlab.com/tangibleai/django-delvin/-/blob/main/docs/intent-recognition-overview.md) to understand the context of this file and function.  Consider the "Request Object" section in particular.
  * [x] Update the code on line 130 (or any other necessary code that is contributing to that error) to make sure the code runs without an error message.

**Remaining Sprint Tasks**
- Issue 2
  * [ ] Make a [Landbot account](https://landbot.io/)
  * [ ] The Landbot integration is throwing a [500 error](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#5xx_server_errors).  Diagnose the cause of this error and try to correct it.
      ```
      Nov 17 01:54:19 PM  10.204.127.219 - - [17/Nov/2022:04:54:19 +0000] "POST /api/turn/label/ HTTP/1.1" 500 96025 "-" "Turn/4.282.1"
      ```
  * [ ] Review the [Landbot Message Hook documentation](https://gitlab.com/tangibleai/django-delvin/-/blob/main/docs/landbot-webhook.md) to understand the context of this function.
  * [ ] Use this Landbot chatbot to test your fix.  [Sample Landbot](https://landbot.pro/v3/H-1421823-ZGLC9V27V1SPTS1B/index.html)
    - This Landbot is hooked up to the site, so you don't need to follow the directions to make your own (unless you want to).
    - The Landbot is limited to 100 conversations per month.  If you leave the tab open and continue chatting with it as you work, it will only count as 1 conversation.  Closing the conversation and visiting the link again will count as a separate conversation.
