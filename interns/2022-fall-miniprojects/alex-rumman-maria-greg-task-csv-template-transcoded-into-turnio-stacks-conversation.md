## Exercise background
We create our chatbot design according to a Google Sheets template such as [this one](https://docs.google.com/spreadsheets/d/1MSU6omCxy_VQCeemK_abMPM7yo58Yk0emQUw7vjzHnQ/edit?usp=sharing).

You can see the template [here](https://docs.google.com/spreadsheets/d/1I1980cuusj_r1TjUJyqLrkVq47UVS2iXTBHDh59tdgQ/edit?usp=sharing) and a format explanation [here](https://tan.sfo2.digitaloceanspaces.com/videos/consulting/tangibleai-design-template-explanation.mp4):

Your input is going to be a csv export of the Google sheets (sample input: )
Your output is going to be a Stack file script (sample output:)


## Syntax elements:

#### Element 0: Start and end
Every Turn.io stack should start with 
```
stack Onboarding_AF do
```
and end with
```
end
```



#### Element 1: Text message followed by another text message that is on next line 

CSV Input: 
`
|Line Number | Bot Says              | User Says | Go To |
| ------------ | ---------------------- | ----------- | ------ |
|3           | Thanks for sharing this with me. |      |      |
| 4 | It's important to try and reframe how we view our events| |      |      |


Turn.io Stacks Output: 
```
card Card3, then: Card4 do
    text("""
    Thanks for sharing this with me.
    """)

  end

card Card3, then: Card4 do
  ...

```



#### Element 2: Text message followed by buttons (including default reply)
CSV Input: 

|Line Number | Bot Says              | User Says | Go To |
| ------------ | ---------------------- | ----------- | ------ |
|5           | Does this make sense? |      |      |
| 6 | |[Yes!] |  I'm glad you're following along!     |     
| 7 | |[Not really...] |  That's okay! We can go over this.  | 
|8           | I'm glad you're following along!|      |Sometimes, due to our self-talk, we tend to view things in a much more negative light.      |
|9           | That's okay! We can go over this. |      |  Sometimes, due to our self-talk, we tend to view things in a much more negative light.    |



Turn.io Stacks Output: 
```
  card Card5, then: Card5default do
    buttons([Button6, Button7]) do
      text("""
      Does this make sense? 
      """)
    end
  end

  card Button6, "Yes!", then: Card8 do
    ## Comment: we can't put nothing inside the card so we're putting a log command. We can work on meaningful commands later
    log("Placeholder")
  end

  card Button7, "Not really...", then: Card9 do
    log("Placeholder")
  end

  ## Default card - if user didn't hit one of the buttons, present an error message and then present the card again
   card Card5default, then: Card5 do
    text("Please use one of my buttons")
  end

  ## Note that if there is something in the "Go to" column of a text message, this should determine the next card to go to
  card Card8, then: Card10 do
    text("""      
		I'm glad you're following along!
    """)
  end

  card Card9, then: Card10 do
    text("""      
		That's okay! We can go over this.
    """)
  end
```
#### Element 3: Open question 

CSV Input: 
|Line Number | Bot Says              | User Says | Go To |Save to Variable|
| ------------ | ---------------------- | ----------- | ------ |---------|
|5  | Type out a short statement that can describe your situation. Be as blunt as you can.|      |      ||
| 6 | | < user input >| Great. Let's see if we can reframe this into a more positive light     | statement  |  
| 7 | Great. Let's see if we can reframe this into a more positive light| |  | 

Turn.io Stacks Output: 
```
  card Card5, then: Card7 do
    response =
      ask("""
      Type out a short statement that can describe your situation. Be as blunt as you can.
      """)
    update_contact(statement: "@response")
  end

card Card7, then: Card8 do
    text("""      		  
      Great. Let's see if we can reframe this into a more positive light.
    """)
  end
```
