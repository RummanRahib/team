# Understanding Linear Regression

In this lesson you'll create your first machine learning model by hand.

## Human Learning

In fact, we should call it human learning, or a human guesswork.
You won't need to do any coding or math to build this model.
The machine isn't doing any of the learning or training.
You are just going to try a bunch a models and see which one gives the best preditions.

You'll be using the same basic approach that a machine would use.
Most machine learning algorithms rely on gradient descent to try to find the best model.
At first you'll just use your instinct to play around with this linear regression model.
Try several different values for the slope and intercept to see if you can find a combination works well.

What does "works well" mean?
How can you tell whether one model is better than another?

<!--
    0. dataset with 5 points and values like 1.0 2.0 5.0 10.0 0.5 8.5 (for both x and y)
    0. real pro-social problem like predicting spamminess or literariness from text length or word length or compressed file size
    0. add a next button, which turns green when RMSE gets low
    1. reveal the text box for the intercept first, slope 0
    2. reveal the text box for the slope
    3. reveal the sliders and the optimal RMSE value, with the formula to reveal the difference
    4. reveal the optimal intercept
    5. reveal the optimal slope and the correct value for the intercept first
-->

<!-- use real data from that UFL file dataset, with features that need quadratic and threshold and offset/landmark features -->
<!-- remove sliders from exercise and force student to type several numbers and hand calculate gradients and parameters -->
<!-- add polynomial features -->
<!-- add additional demographic features -->
<!-- add additional error metrics -->
<!-- add normalize button for error metrics -->
<!-- create accuracy or correlation score metric -->

In the real world this is an import question to ask your client.
You want to make sure the model will work well for them and their users.
But for now we're just going to use Root Mean Square Error (RMSE) as an estimate for how bad your model is doing.
Play with the sliders and numeric values here to try to get the RMSE as low as possible.
playground.proai.org

## Machine Learning

So why do people call this machine learning.
Because it's possible for a machine to find out (learn) what the best model is, the same way you did.
An algorithm can adjust the slope and intercept values just like you did.

## Human Learning 

Try this exercise.
See if you can "learn" a predictive model for these 8 points of data in our [Machine Learning Playground](playground.proai.org).
What is the smallest RMSE (Root Mean Square Error) you can achieve?
Try adjusting the slope or intercept parameters one at a time.
Watch to see how much the RMSE changes for each increment of parameter change.
Can you estimate how much you would have to change that parameter to acheive zero RMSE error?
Remember zero error and 100% accuracy is not possible for this problem because a line cannot pass through all the points.
So what if you targeted an error of 1.
Can you come up with an algorithm, a procedure, that will get you to the smallest (minimum) error with the fewest number of parameter adjustments?
This algorithm you are imagining is what is called a gradient descent or steepest descent optimization algorithm.
You are optimizing the accuracy by minimizing the loss or error or RMSE.
