# Some Spectacular Success at Openness to what the Data Says

Listening to data is a great way to make a billion dollars.

## Zoom

- one-click launch
- no encryption
- peer-to-peer

<--! Talk to Aira java devleoper about his video streaming project -->

## Slack

- Magic Link: first to use passwordless e-mail as the sole authorization mechanism (no passwords)
- Slack invite links, expire in 30 days (balance between trolls and good people joining a workspace)
